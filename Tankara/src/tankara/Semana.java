package tankara;

/**
 *
 * @author mramoss
 */
public class Semana {

    private int numero;
    double tasaRECt, tasaARRt, tasaESCt, nGt, mSt;

    public Semana(int numero, double tasaRECt, double tasaARRt, double tasaESCt, double nGt, double mSt) {
        this.numero = numero;
        this.tasaRECt = tasaRECt;
        this.tasaARRt = tasaARRt;
        this.tasaESCt = tasaESCt;
        this.nGt = nGt;
        this.mSt = mSt;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getTasaRECt() {
        return tasaRECt;
    }

    public void setTasaRECt(double tasaRECt) {
        this.tasaRECt = tasaRECt;
    }

    public double getTasaARRt() {
        return tasaARRt;
    }

    public void setTasaARRt(double tasaARRt) {
        this.tasaARRt = tasaARRt;
    }

    public double getTasaESCt() {
        return tasaESCt;
    }

    public void setTasaESCt(double tasaESCt) {
        this.tasaESCt = tasaESCt;
    }

    public double getnGt() {
        return nGt;
    }

    public void setnGt(double nGt) {
        this.nGt = nGt;
    }

    public double getmSt() {
        return mSt;
    }

    public void setmSt(double mST) {
        this.mSt = mSt;
    }

}
