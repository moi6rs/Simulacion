package tankara;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author mramoss
 */
public class Tankara {

    private int nroSemana;
    private int target;

    public Tankara(int nroSemana, int target) {
        this.nroSemana = nroSemana;
        this.target = target;
    }

    public int getNroSemana() {
        return nroSemana;
    }

    public void setNroSemana(int nroSemana) {
        this.nroSemana = nroSemana;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    private double tasaRECt(int target, double mSt) {
        double tasaRECt = (target - mSt) / 4;
        return tasaRECt;
    }

    private double tasaARRt(double mSt) {
        double tasaARRt = mSt * 0.05;
        return tasaARRt;
    }

    private double tasaESCt(double nGt) {
        double tasaESCt = nGt / 10;
        return tasaESCt;
    }

    private double nGt(double nGtAnterior, double tasaARRt, double tasaESCt) {
        double nGt = nGtAnterior + (tasaARRt - tasaESCt);
        return nGt;
    }

    private double mSt(double tasaRECt, double tasaARRt, double tasaESCt, double mSTAnterior) {
        double mSt = mSTAnterior + (tasaRECt - tasaARRt) + tasaESCt;
        return mSt;
    }

    public ArrayList<Semana> armarTabla() {
        ArrayList<Semana> semanas = new ArrayList<Semana>();
        Semana newSemana = new Semana(0, 0, 0, 0, 0, 0);
        double tasaRECtN = 0;
        double tasaARRtN = 0;
        double tasaESCtN = 0;
        double nGtN = 0;
        double mStN = 0;
        for (int i = 0; i <= getNroSemana(); i++) {
            if (0 == i) {
                semanas.add(newSemana);
            } else {
                tasaRECtN = tasaRECt(target, semanas.get(i - 1).getmSt());
                tasaARRtN = tasaARRt(semanas.get(i - 1).getmSt());
                tasaESCtN = tasaESCt(semanas.get(i - 1).getnGt());
                nGtN = nGt(semanas.get(i - 1).getnGt(), tasaARRtN, tasaESCtN);
                mStN = mSt(tasaRECtN, tasaARRtN, tasaESCtN, semanas.get(i - 1).getmSt());
                Semana semanaN = new Semana(i, tasaRECtN, tasaARRtN, tasaESCtN, nGtN, mStN);
                semanas.add(semanaN);
            }
        }
        return semanas;
    }

    public void mostrarTabla() {
        ArrayList<Semana> semanas = armarTabla();
        Semana semanaN = null;
        System.out.println("Sem"+"   "+"RECt"+"   "+"ARRt"+"   "+"ESCt"+"   "+"NGt"+"   "+"MSt");
        for (int i = 0; i <= getNroSemana(); i++) {
            semanaN = semanas.get(i);
            System.out.print(semanaN.getNumero());
            System.out.print("   ");
            System.out.print(redondear2(semanaN.getTasaRECt()));
            System.out.print("   ");
            System.out.print(redondear2(semanaN.getTasaARRt()));
            System.out.print("   ");
            System.out.print(redondear2(semanaN.getTasaESCt()));
            System.out.print("   ");
            System.out.print(redondear2(semanaN.getnGt()));
            System.out.print("   ");
            System.out.print(redondear2(semanaN.getmSt()));
            System.out.print("\n");
        }
    }

    private double redondear(double nro) {
        double nroRedondeado = 0;
        DecimalFormat formato1 = new DecimalFormat("0.00");
        nroRedondeado = (double) Math.round(nro * 100d) / 100d;
        return nroRedondeado;
    }

    private String redondear2(double nro) {
        String nroRedondeado = " ";
        DecimalFormat formato1 = new DecimalFormat("0.00");
        nroRedondeado = formato1.format(nro);
        return nroRedondeado;
    }
}
