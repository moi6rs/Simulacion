package generador;

import java.awt.print.Book;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author mramoss
 */
public class CongruencialMixto {

    private double nuneroA;
    private double numeroXn;
    private double numeroC;
    private double numeroM;
    private double numeroXo;

    public CongruencialMixto(double nuneroA, double numeroXn, double numeroC,
            double numeroM, double numeroXo) {
        this.nuneroA = nuneroA;
        this.numeroXn = numeroXn;
        this.numeroC = numeroC;
        this.numeroM = numeroM;
        this.numeroXo = numeroXo;
    }

    public double getNuneroA() {
        return nuneroA;
    }

    public void setNuneroA(double nuneroA) {
        this.nuneroA = nuneroA;
    }

    public double getNumeroXn() {
        return numeroXn;
    }

    public void setNumeroXn(double numeroXn) {
        this.numeroXn = numeroXn;
    }

    public double getNumeroC() {
        return numeroC;
    }

    public void setNumeroC(double numeroC) {
        this.numeroC = numeroC;
    }

    public double getNumeroM() {
        return numeroM;
    }

    public void setNumeroM(double numeroM) {
        this.numeroM = numeroM;
    }

    public double getNumeroXo() {
        return numeroXo;
    }

    public void setNumeroXo(double numeroXo) {
        this.numeroXo = numeroXo;
    }

    public ArrayList<Double> generar() {

        Double numeroNmasUno = 0.0;
        ArrayList<Double> Xn = new ArrayList<>();
        ArrayList<Double> nroAleatorio = new ArrayList<>();

        for (int n = 0; n < numeroM; n++) {
            if (n == 0) {
                numeroNmasUno = (nuneroA * numeroXo + numeroC) % numeroM;
                Xn.add(numeroNmasUno);
                nroAleatorio.add(numeroNmasUno / numeroM);
            }
            if (n > 0) {
                numeroNmasUno = (nuneroA * Xn.get(n - 1) + numeroC) % numeroM;
                Xn.add(numeroNmasUno);
                nroAleatorio.add(numeroNmasUno / numeroM);
            }
            System.out.println("Nro. Aleatorio Mixto : " + (numeroNmasUno / numeroM));
        }
        return nroAleatorio;
    }

    public int longitud() {
        return 0;

    }

    private String redondear(double nro) {
        String nroRedondeado = " ";
        DecimalFormat formato1 = new DecimalFormat("0.00000");
        nroRedondeado = formato1.format(nro);
        return nroRedondeado;
    }
}
