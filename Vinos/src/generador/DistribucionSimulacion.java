/*
https://github.com/ernesto1989/GeneradorDistribucionesAleatorias/blob/master/src/main/java/com/aleph5/RandomVariableGenerator/GeneradorDistribucionesAleatorias.java
 */
package generador;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mramoss
 */
public class DistribucionSimulacion {

    public DistribucionSimulacion() {
    }

    public static Double generadorDistribucionUniformeAB(double a, double b, double nroAleatorioR1) {
        double uniformeAB = a + ((b - a) * nroAleatorioR1);
        return uniformeAB;
    }

    public static Double monteCarlo(Double nroAleatorio) {
        Double porcentajeUvasDescartadas = 0.0;
        //% de uvas descartadas y Probabilidad 
        if (0.0 > nroAleatorio && nroAleatorio <= 0.15) {
            porcentajeUvasDescartadas = 0.01;
        } else if (0.15 > nroAleatorio && nroAleatorio <= 0.45) {
            porcentajeUvasDescartadas = 0.02;
        } else if (0.45 > nroAleatorio && nroAleatorio <= 0.70) {
            porcentajeUvasDescartadas = 0.035;
        } else if (0.70 > nroAleatorio && nroAleatorio <= 0.85) {
            porcentajeUvasDescartadas = 0.05;
        } else if (0.85 > nroAleatorio && nroAleatorio <= 0.95) {
            porcentajeUvasDescartadas = 0.07;
        } else if (0.95 > nroAleatorio && nroAleatorio <= 1) {
            porcentajeUvasDescartadas = 0.1;
        }
        return porcentajeUvasDescartadas;
    }

    public static Double distribucionTriangular(Double valorMax, Double valorPromedio,
        Double valorMin, Double nroAleatorioR1, Double nroAleatorioR2) {
        //Applicando Transformada inversa
        Double variableX = 0.0;
        Double valor = (valorPromedio - valorMin) / (valorMax - valorMin);
        if (nroAleatorioR1 < valor) {
            variableX = valorMin + ((valorPromedio - valorMin) * Math.sqrt(nroAleatorioR2));
        } else {
            variableX = valorMax-((valorMax-valorPromedio)*Math.sqrt(1-nroAleatorioR2));
        }
        return variableX;
    }
     public static Double monteCarloDemandaVinos(Double nroAleatorioR1, 
             ArrayList<Double> litrosVino, ArrayList<Double> probVino) {
        Double porcentajeVino = 0.0;
         System.out.println("generador.DistribucionSimulacion.monteCarloDemandaVinos()");
         System.out.println(nroAleatorioR1);
        //listros de vino y Probabilidad 
        double uno = probVino.get(0);
        double dos = probVino.get(0)+probVino.get(1);
        System.out.println(uno);
        System.out.println(dos);
        
        if ( nroAleatorioR1 > 0.0  && nroAleatorioR1 <= uno) {
            porcentajeVino = litrosVino.get(0);
        } else if (nroAleatorioR1 > uno && nroAleatorioR1 <= dos) {
            porcentajeVino = litrosVino.get(1);
        }else if (nroAleatorioR1 > dos && nroAleatorioR1 <= 1.00) {
            porcentajeVino = litrosVino.get(2);
        }
        return porcentajeVino;
     }

}
