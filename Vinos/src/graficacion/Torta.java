package graficacion;

import java.io.*;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author mramoss
 */
public class Torta {

    public void graficar(Double datoUno,Double datoDos, Double datoTres){
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Opcion Uno", datoUno);
        dataset.setValue("Opcion Dos", datoDos);
        dataset.setValue("Opcion Tres", datoTres);

        JFreeChart grafica = ChartFactory.createPieChart3D(
                "Total Utilidad Vinos", // chart title                   
                dataset, // data 
                true, // include legend                   
                true,
                false);      
        ChartPanel contenedor = new ChartPanel(grafica);
        JFrame ventana = new JFrame("Grafica Utilidad de Opciones");
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.add(contenedor);
        ventana.setSize(600,500);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
             
    }
}
