package produccionVinos;

import generador.CongruencialMixto;
import generador.CongruencialMultiplicativo;
import generador.DistribucionSimulacion;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mramoss
 */
public class Cosecha {
    private String produccion;
    private Double tamanioTerreno;
    private Double prodPorHectaria;
    private Double aleatorioUno;
    private Double nroAleatorioDos;

    public Cosecha(String produccion, Double tamanioTerreno, Double prodPorHectaria, Double aleatorioUno, Double nroAleatorioDos) {
        this.produccion = produccion;
        this.tamanioTerreno = tamanioTerreno;
        this.prodPorHectaria = prodPorHectaria;
        this.aleatorioUno = aleatorioUno;
        this.nroAleatorioDos = nroAleatorioDos;
    }

    public String getProduccion() {
        return produccion;
    }

    public void setProduccion(String produccion) {
        this.produccion = produccion;
    }

    public Double getTamanioTerreno() {
        return tamanioTerreno;
    }

    public void setTamanioTerreno(Double tamanioTerreno) {
        this.tamanioTerreno = tamanioTerreno;
    }

    public Double getProdPorHectaria() {
        return prodPorHectaria;
    }

    public void setProdPorHectaria(Double prodPorHectaria) {
        this.prodPorHectaria = prodPorHectaria;
    }

    public Double getAleatorioUno() {
        return aleatorioUno;
    }

    public void setAleatorioUno(Double aleatorioUno) {
        this.aleatorioUno = aleatorioUno;
    }

    public Double getNroAleatorioDos() {
        return nroAleatorioDos;
    }

    public void setNroAleatorioDos(Double nroAleatorioDos) {
        this.nroAleatorioDos = nroAleatorioDos;
    }
    
    public Double prodTotalBruta(){
        Double prod = tamanioTerreno*prodPorHectaria;
        return  prod;
    }
    public Double prodTotal(){
        double  prod = prodTotalBruta() + calcularSobreProd();
        return prod;
    }
    public Double prodTotalEstimada(){
        Double  prod = prodTotal() - calcularPerdidaProd();
        return prod;
    }
    public Double aleatorioDos(){        
        return this.nroAleatorioDos;
    }
    public Double aleatorioUno(){
        return this.aleatorioUno;
    }
    
    public Double calcularSobreProd(){
        Double kgSobreProd;
        double nroAleatorio = aleatorioUno();
        if(0.0>nroAleatorio && 0.2 <= nroAleatorio){
            kgSobreProd = (prodTotal()*nroAleatorio);
        }else{
            kgSobreProd = 0.0;
        }
        return kgSobreProd;
    }
    public Double calcularPerdidaProd(){
        DistribucionSimulacion distribucion = new DistribucionSimulacion();
        double perdidaProd = distribucion.monteCarlo(aleatorioDos());
        return perdidaProd;
    }    
}
