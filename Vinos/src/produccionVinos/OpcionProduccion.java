package produccionVinos;

/**
 *
 * @author mramoss
 */
public class OpcionProduccion {
    private String tipoVino;
    private Double demandaEstimada;
    private Double prodRealVinoMaduro;
    private Double prodSatisfacerDemanda;
    private Double diferencia;
    private Double utilidadAjuste;
    private String observacion;

    public OpcionProduccion(String tipoVino, Double demandaEstimada, Double prodRealVinoMaduro, Double prodSatisfacerDemanda, Double diferencia, Double utilidadAjuste, String observacion) {
        this.tipoVino = tipoVino;
        this.demandaEstimada = demandaEstimada;
        this.prodRealVinoMaduro = prodRealVinoMaduro;
        this.prodSatisfacerDemanda = prodSatisfacerDemanda;
        this.diferencia = diferencia;
        this.utilidadAjuste = utilidadAjuste;
        this.observacion = observacion;
    }
    
    public String getTipoVino() {
        return tipoVino;
    }

    public void setTipoVino(String tipoVino) {
        this.tipoVino = tipoVino;
    }

    public Double getDemandaEstimada() {
        return demandaEstimada;
    }

    public void setDemandaEstimada(Double demandaEstimada) {
        this.demandaEstimada = demandaEstimada;
    }

    public Double getProdRealVinoMaduro() {
        return prodRealVinoMaduro;
    }

    public void setProdRealVinoMaduro(Double prodRealVinoMaduro) {
        this.prodRealVinoMaduro = prodRealVinoMaduro;
    }

    public Double getProdSatisfacerDemanda() {
        return prodSatisfacerDemanda;
    }

    public void setProdSatisfacerDemanda(Double prodSatisfacerDemanda) {
        this.prodSatisfacerDemanda = prodSatisfacerDemanda;
    }

    public Double getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Double diferencia) {
        this.diferencia = diferencia;
    }

    public Double getUtilidadAjuste() {
        return utilidadAjuste;
    }

    public void setUtilidadAjuste(Double utilidadAjuste) {
        this.utilidadAjuste = utilidadAjuste;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    
    
}
