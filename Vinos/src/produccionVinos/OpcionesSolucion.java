package produccionVinos;

/**
 *
 * @author mramoss
 */
public class OpcionesSolucion {

    private String nombreVino;
    private double demandaVino;
    private double producionVino;

    public OpcionesSolucion(String nombreVino, double demandaVino, double producionVino) {
        this.nombreVino = nombreVino;
        this.demandaVino = demandaVino;
        this.producionVino = producionVino;    
    }

    public String getNombreVino() {
        return nombreVino;
    }

    public void setNombreVino(String nombreVino) {
        this.nombreVino = nombreVino;
    }

    public double getDemandaVino() {
        return demandaVino;
    }

    public void setDemandaVino(double demandaVino) {
        this.demandaVino = demandaVino;
    }

    public double getProducionVino() {
        return producionVino;
    }

    public void setProducionVino(double producionVino) {
        this.producionVino = producionVino;
    }
}
