package pruebas;

import java.util.ArrayList;

/**
 *
 * @author mramoss
 */
public class Normal {

    private double estadisticoTeorico;
    private ArrayList<Double> muestra;

    public Normal(double estadisticoTeorico, ArrayList<Double> muestra) {
        this.estadisticoTeorico = estadisticoTeorico;
        this.muestra = muestra;
    }

    public double getEstadisticoTeorico() {
        return estadisticoTeorico;
    }

    public void setEstadisticoTeorico(double estadisticoTeorico) {
        this.estadisticoTeorico = estadisticoTeorico;
    }

    public ArrayList<Double> getMuestra() {
        return muestra;
    }

    public void setMuestra(ArrayList<Double> muestra) {
        this.muestra = muestra;
    }

    //PASO 1 contrar con la muestra y el tamanio
    public int tamanioMuestro() {
        return muestra.size();
    }

    //PASO 2 clacular X promedio
    public double calcularX() {
        double promedio = 0.0;
        double suma = 0.0;
        for (int i = 0; i < muestra.size(); i++) {
            suma += muestra.get(i);
        }
        return promedio = suma / tamanioMuestro();
    }

    //PASO 3 Calcular Z
    public double calcularZo() {
        double calculoZo = ((1 / 2) - calcularX() * (Math.sqrt(tamanioMuestro()))) / (Math.sqrt(1 / 12));
        return calculoZo;
    }

    //Paso 4 Calcular Estadistico
    public double estadisticoZ() {
        return getEstadisticoTeorico();
    }

    //Paso 5 Estableces
    //Paso 6 Concluir
    public String conclusion() {
        String respuesta = "";
        if (calcularZo() < estadisticoTeorico) {
            respuesta = "Uniforme";
        } else {
            respuesta = "No Uniforme";
        }
        return respuesta;
    }

}
