package ventanas;

import com.sun.security.auth.module.JndiLoginModule;
import generador.CongruencialMixto;
import javax.swing.table.DefaultTableModel;
import generador.DistribucionSimulacion;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import produccionVinos.Cosecha;
import produccionVinos.CostoUtilidad;
import produccionVinos.MacFerMad;

/**
 *
 * @author mramoss
 */
public final class Principal extends javax.swing.JFrame {

    DefaultTableModel tablaCosecha = new DefaultTableModel();
    DefaultTableModel tablaMacFerMad = new DefaultTableModel();
    DefaultTableModel tablaCostoUtilidad = new DefaultTableModel();
    public ArrayList<Double> nrosAleatorios = new ArrayList<>();

    public Principal() {
        initComponents();
        ejecutarTablaCosecha();
        ejecutarTablaMacFerMad();
        ejecutarTablaCostoUtilidade();
        setNrosAleatorios();

    }

    public void setNrosAleatorios() {
        ArrayList<Double> nrosAle = new ArrayList<>();
        CongruencialMixto newCongruencialMixto = new CongruencialMixto(61.0, 11.0, 421.0, 1000.0, 11.0);
        this.nrosAleatorios = newCongruencialMixto.generar();
    }

    private void ejecutarTablaCosecha() {
        String[] cabecera = new String[]{"Producto", "Hectária",
            "Prod. 1 Hectária Kg", "Producción Bruta Kg", "Varible 1",
            "Sobreproducion", "Total Prod.", "Variable 2", "Pérdida Estimada Kg",
            "Prod. Total Estimada"
        };
        tablaCosecha.setColumnIdentifiers(cabecera);
        jTableCosecha.setModel(tablaCosecha);
    }

    void agregarDatosTablaCosecha() {
        String tamanioTerreno = jTextFieldTerreno.getText();
        double tTerreno = Double.parseDouble(tamanioTerreno);
        String prodHectaria = jTextFieldProdHectaria.getText();
        double prodUvaHectaria = Double.parseDouble(prodHectaria);
        int numero = ThreadLocalRandom.current().nextInt(0, 998 + 1);
        Cosecha cosecha = new Cosecha("Uva", tTerreno, prodUvaHectaria, nrosAleatorios.get(0), nrosAleatorios.get(numero));

        tablaCosecha.addRow(new Object[]{
            "Uva",
            cosecha.getTamanioTerreno(),
            cosecha.getProdPorHectaria(),
            cosecha.prodTotalBruta(),
            cosecha.aleatorioUno(),
            cosecha.calcularSobreProd(),
            cosecha.prodTotal(),
            cosecha.aleatorioDos(),
            cosecha.calcularPerdidaProd(),
            cosecha.prodTotalEstimada()
        });
        cargarTablaMacFerMad(cosecha.prodTotalEstimada());
        cargarTablaCostoUtilidad();

    }

    private void cargarTablaMacFerMad(double uvasProcesar) {
        ArrayList<String> tipoVinos = new ArrayList<>();
        tipoVinos.add("Espumantes");
        tipoVinos.add("Joven/Añejo");
        //tipoVinos.add("Añejados en Roble");

        String nombreVino = "";
        for (int i = 0; i < tipoVinos.size(); i++) {
            nombreVino = tipoVinos.get(i);
            agregarDatosTablaMacFerMad(uvasProcesar, nombreVino);
        }

    }

    private ArrayList<Double> preciosVentaVinos() {
        ArrayList<Double> preciosVenta = new ArrayList<>();

        String puEspumoso = jTextFieldPUVinoEspumoso.getText();
        double precioUdEspumoso = Double.parseDouble(puEspumoso);
        String puJoven = jTextFieldPUVinoJoven.getText();
        double precioUdJoven = Double.parseDouble(puJoven);
        String puAnejo = jTextFieldPUVinoAnejo.getText();
        double precioUdAnejo = Double.parseDouble(puAnejo);

        preciosVenta.add(precioUdEspumoso);
        preciosVenta.add(precioUdJoven);
        preciosVenta.add(precioUdAnejo);
        return preciosVenta;
    }

    private ArrayList<Double> costosVariablesVinos() {
        ArrayList<Double> costosVariable = new ArrayList<>();
        String cvEspumoso = jTextFieldCostoVariableVinoEspumoso.getText();
        double costoVarEspumoso = Double.parseDouble(cvEspumoso);
        String cvJoven = jTextFieldCostoVariableVinoJoven.getText();
        double costoVarJoven = Double.parseDouble(cvJoven);
        String cvAnejo = jTextFieldCostoVariableVinoAnejo.getText();
        double costoVarAnejo = Double.parseDouble(cvAnejo);

        costosVariable.add(costoVarEspumoso);
        costosVariable.add(costoVarJoven);
        costosVariable.add(costoVarAnejo);
        return costosVariable;
    }

    private void ejecutarTablaMacFerMad() {
        String[] cabecera = new String[]{"Tipos de Vino", "Uvas a Procesar Kg",
            "% Uvas Kg", "Tiempo Fermentacion Días", "Rendimiento 750-830 Lts",
            "Fermentación Lts", "Aleatorio", "Maduracion Lts"
        };
        tablaMacFerMad.setColumnIdentifiers(cabecera);
        jTableMacFerMad.setModel(tablaMacFerMad);
    }

    private void agregarDatosTablaMacFerMad(double uvasProcesar, String nombreVino) {
        int numero = ThreadLocalRandom.current().nextInt(0, 998 + 1);
        MacFerMad macFerMad = new MacFerMad(nombreVino, uvasProcesar, nrosAleatorios.get(numero));
        double maduro = macFerMad.maduracion();
        tablaMacFerMad.addRow(new Object[]{
            macFerMad.getTipoVino(),
            macFerMad.getUvasPorcesar(),
            redondear(macFerMad.porcentajeUvas()),
            macFerMad.tiempoFermentacion(),
            macFerMad.rendimiento(),
            redondear(macFerMad.resultadoFermentacion()),
            redondear(macFerMad.nroAleatorio()),
            redondear(maduro)
        });
    }

    private void cargarTablaCostoUtilidad() {
        ArrayList<String> tipoVinos = new ArrayList<>();
        tipoVinos.add("Espumantes");
        tipoVinos.add("Jovenes");
        tipoVinos.add("Añejados");

        ArrayList<Double> costosVariable = costosVariablesVinos();
        ArrayList<Double> preciosVenta = preciosVentaVinos();
        ArrayList<Double> vinosMaduros = vinosMadurosLitros();

        String costoFijo = jTextFieldCostoFijo.getText();
        double costoFijoVinos = Double.parseDouble(costoFijo);

        String nombreVino = "";
        for (int i = 0; i < tipoVinos.size(); i++) {
            nombreVino = tipoVinos.get(i);
            agregarDatosTablaCostoUtilidad(nombreVino, vinosMaduros.get(i), costoFijoVinos, costosVariable.get(i), preciosVenta.get(i));
        }
    }

    private ArrayList<Double> vinosMadurosLitros() {
        ArrayList<Double> vinosMaduros = new ArrayList<>();

        if (tablaMacFerMad.getRowCount() > 0) {
            String utilidadEspumantes = String.valueOf(tablaMacFerMad.getValueAt(0, 7));
            double valorEspumantes = Double.parseDouble(utilidadEspumantes.replace(",", "."));
            String utilidadJovenesAnejos = String.valueOf(tablaMacFerMad.getValueAt(1, 7));
            double valorJovenesAnejos = Double.parseDouble(utilidadJovenesAnejos.replace(",", "."));
            vinosMaduros.add(valorEspumantes);
            vinosMaduros.add(valorJovenesAnejos);
            vinosMaduros.add(valorJovenesAnejos);
        } else {
            JOptionPane.showMessageDialog(null, "Debe Simular Primero..!!");
        }
        return vinosMaduros;
    }

    void agregarDatosTablaCostoUtilidad(String tipoVino, Double vinoMadurado, Double costoFijo, Double costoVariable, Double precioUnitarioVenta) {
        CostoUtilidad costoUtilidad = new CostoUtilidad(tipoVino, vinoMadurado,
                costoFijo, costoVariable, precioUnitarioVenta);

        tablaCostoUtilidad.addRow(new Object[]{
            costoUtilidad.getTipoVino(),
            redondear(costoUtilidad.getVinoMadurado()),
            costoUtilidad.getCostoFijo(),
            costoUtilidad.getCostoVariable(),
            costoUtilidad.getPrecioUnitarioVenta()

        });
    }

    private void ejecutarTablaCostoUtilidade() {
        String[] cabecera = new String[]{"Tipos de Vino", "Vino Madurado L",
            "Costo Fijo", "Costo Variable por L",
            "Precio Unitario de Venta"
        };
        tablaCostoUtilidad.setColumnIdentifiers(cabecera);
        jTableCostoUtilidad.setModel(tablaCostoUtilidad);
    }

    private String redondear(double nro) {
        String nroRedondeado = " ";
        DecimalFormat formato1 = new DecimalFormat("0.00");
        nroRedondeado = formato1.format(nro);
        return nroRedondeado;
    }

    private void eliminarDatosTabla(DefaultTableModel tablaDatos) {
        if (tablaDatos.getRowCount() > 0) {
            int filas = tablaDatos.getRowCount();
            for (int i = 0; i < filas; i++) {
                tablaDatos.removeRow(i);
                i -= 1;
            }
            System.out.println("Datos Eliminados de Tabla..");
        } else {
            System.out.println("La tabla ya esta vacia..!!");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jFrame1 = new javax.swing.JFrame();
        jLabel5 = new javax.swing.JLabel();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        panel1 = new java.awt.Panel();
        jLabelPUVinoEspumoso = new javax.swing.JLabel();
        jTextFieldTerreno = new javax.swing.JTextField();
        jTextFieldCostoFijo = new javax.swing.JTextField();
        jTextFieldPUVinoJoven = new javax.swing.JTextField();
        jTextFieldPUVinoEspumoso = new javax.swing.JTextField();
        jTextFieldPUVinoAnejo = new javax.swing.JTextField();
        labelConstantes = new java.awt.Label();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCosecha = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableMacFerMad = new javax.swing.JTable();
        labelMacFerMad = new java.awt.Label();
        labelMacFerMad1 = new java.awt.Label();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableCostoUtilidad = new javax.swing.JTable();
        jLabelProdHectaria = new javax.swing.JLabel();
        jTextFieldProdHectaria = new javax.swing.JTextField();
        jButtonPorDefecto = new javax.swing.JButton();
        jButtonSimular = new javax.swing.JButton();
        jButtonVaciar = new javax.swing.JButton();
        labelCVVinoJoven = new java.awt.Label();
        jTextFieldCostoVariableVinoJoven = new javax.swing.JTextField();
        jTextFieldCostoVariableVinoEspumoso = new javax.swing.JTextField();
        jTextFieldCostoVariableVinoAnejo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jButtonSoluciones = new javax.swing.JButton();
        jLabelTerreno = new javax.swing.JLabel();
        jLabelFondo1 = new javax.swing.JLabel();
        jLabelCVVinoAnejo = new javax.swing.JLabel();
        jLabelPUVinoJoven1 = new javax.swing.JLabel();
        jLabelPUVinoEspumoso1 = new javax.swing.JLabel();
        jLabelCostoFijo2 = new javax.swing.JLabel();
        jLabelCVVinoEspumoso = new javax.swing.JLabel();
        jLabelFondo67 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu6 = new javax.swing.JMenu();
        jMenuMixto = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jLabel5.setText("jLabel5");

        jMenu1.setText("jMenu1");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Pruduccion Vinos");
        setBackground(new java.awt.Color(204, 204, 255));
        setForeground(new java.awt.Color(204, 204, 255));
        setName("frameSimulacionProduccionVinos"); // NOI18N
        setSize(new java.awt.Dimension(1080, 720));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel1.setBackground(new java.awt.Color(51, 51, 255));
        panel1.setForeground(new java.awt.Color(102, 102, 255));

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 531, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        getContentPane().add(panel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 97, 531, -1));
        panel1.getAccessibleContext().setAccessibleName("");

        jLabelPUVinoEspumoso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoEspumoso.setText("P/U Vino  Añejo ($)");
        getContentPane().add(jLabelPUVinoEspumoso, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 460, -1, -1));
        jLabelPUVinoEspumoso.getAccessibleContext().setAccessibleName("");

        jTextFieldTerreno.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTerreno.setText("0");
        jTextFieldTerreno.setMinimumSize(new java.awt.Dimension(12, 20));
        jTextFieldTerreno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTerrenoActionPerformed(evt);
            }
        });
        getContentPane().add(jTextFieldTerreno, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 110, 30));
        jTextFieldTerreno.getAccessibleContext().setAccessibleName("jTextFieldTerreno");

        jTextFieldCostoFijo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoFijo.setText("0");
        jTextFieldCostoFijo.setName("jTextFieldCostoFijo"); // NOI18N
        jTextFieldCostoFijo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCostoFijoActionPerformed(evt);
            }
        });
        getContentPane().add(jTextFieldCostoFijo, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 459, 89, -1));
        jTextFieldCostoFijo.getAccessibleContext().setAccessibleName("jTextFieldCostoFijo");

        jTextFieldPUVinoJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoJoven.setText("0");
        jTextFieldPUVinoJoven.setName("jTextFieldPUVinoJoven"); // NOI18N
        getContentPane().add(jTextFieldPUVinoJoven, new org.netbeans.lib.awtextra.AbsoluteConstraints(359, 459, 93, -1));
        jTextFieldPUVinoJoven.getAccessibleContext().setAccessibleName("jTextFieldPUVinoJoven");

        jTextFieldPUVinoEspumoso.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoEspumoso.setText("0");
        jTextFieldPUVinoEspumoso.setName("jTextFieldPUVinoEspumoso"); // NOI18N
        getContentPane().add(jTextFieldPUVinoEspumoso, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 460, 87, -1));
        jTextFieldPUVinoEspumoso.getAccessibleContext().setAccessibleName("jTextFieldPUVinoEspumoso");

        jTextFieldPUVinoAnejo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoAnejo.setText("0");
        jTextFieldPUVinoAnejo.setName("jTextFieldPUVinoAnejo"); // NOI18N
        getContentPane().add(jTextFieldPUVinoAnejo, new org.netbeans.lib.awtextra.AbsoluteConstraints(871, 459, 93, -1));
        jTextFieldPUVinoAnejo.getAccessibleContext().setAccessibleName("jTextFieldPUVinoAnejo");

        labelConstantes.setAlignment(java.awt.Label.CENTER);
        labelConstantes.setBackground(new java.awt.Color(102, 0, 102));
        labelConstantes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelConstantes.setForeground(new java.awt.Color(255, 255, 255));
        labelConstantes.setName("labelConstantes"); // NOI18N
        labelConstantes.setText("SIMULACION PARA LA ETAPA DE COSECHA");
        getContentPane().add(labelConstantes, new org.netbeans.lib.awtextra.AbsoluteConstraints(39, 123, 1150, 30));
        labelConstantes.getAccessibleContext().setAccessibleName("labelConstantes");

        jTableCosecha.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableCosecha.setVerifyInputWhenFocusTarget(false);
        jScrollPane2.setViewportView(jTableCosecha);
        jTableCosecha.getAccessibleContext().setAccessibleName("");
        jTableCosecha.getAccessibleContext().setAccessibleDescription("jTableCosecha");

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, 1150, 60));

        jTableMacFerMad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8"
            }
        ));
        jScrollPane1.setViewportView(jTableMacFerMad);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 320, 1150, 88));

        labelMacFerMad.setAlignment(java.awt.Label.CENTER);
        labelMacFerMad.setBackground(new java.awt.Color(102, 0, 102));
        labelMacFerMad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelMacFerMad.setForeground(new java.awt.Color(255, 255, 255));
        labelMacFerMad.setName("labelConstantes"); // NOI18N
        labelMacFerMad.setText("SIMULACION PARA LA ETAPA DE COSTOS Y UTILIDADES");
        getContentPane().add(labelMacFerMad, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 420, 1150, 30));

        labelMacFerMad1.setAlignment(java.awt.Label.CENTER);
        labelMacFerMad1.setBackground(new java.awt.Color(102, 0, 102));
        labelMacFerMad1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelMacFerMad1.setForeground(new java.awt.Color(255, 255, 255));
        labelMacFerMad1.setName("labelConstantes"); // NOI18N
        labelMacFerMad1.setText("SIMULACION PARA LA ETAPA DE MACERACIÓN, FERMENTACIÓN Y MADURACIÓN");
        getContentPane().add(labelMacFerMad1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 1150, 30));

        jTableCostoUtilidad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5"
            }
        ));
        jScrollPane3.setViewportView(jTableCostoUtilidad);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(39, 527, 840, 102));

        jLabelProdHectaria.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabelProdHectaria.setText("Prod. x Hectaria:");
        getContentPane().add(jLabelProdHectaria, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 170, -1, -1));

        jTextFieldProdHectaria.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProdHectaria.setText("0");
        getContentPane().add(jTextFieldProdHectaria, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 170, 84, -1));

        jButtonPorDefecto.setBackground(new java.awt.Color(0, 153, 51));
        jButtonPorDefecto.setText("Por Defecto");
        jButtonPorDefecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPorDefectoActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonPorDefecto, new org.netbeans.lib.awtextra.AbsoluteConstraints(151, 90, 100, 30));

        jButtonSimular.setBackground(new java.awt.Color(0, 0, 153));
        jButtonSimular.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSimular.setText("Simular");
        jButtonSimular.setToolTipText("");
        jButtonSimular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSimularActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonSimular, new org.netbeans.lib.awtextra.AbsoluteConstraints(258, 90, 100, 30));

        jButtonVaciar.setBackground(new java.awt.Color(255, 0, 51));
        jButtonVaciar.setForeground(new java.awt.Color(255, 255, 255));
        jButtonVaciar.setText("Vaciar");
        jButtonVaciar.setActionCommand("jButtonVaciar");
        jButtonVaciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVaciarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonVaciar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, 100, 30));

        labelCVVinoJoven.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        labelCVVinoJoven.setName("labelPUVinoJoven"); // NOI18N
        labelCVVinoJoven.setText("C. V. Vino Joven ($)");
        getContentPane().add(labelCVVinoJoven, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 500, 115, -1));

        jTextFieldCostoVariableVinoJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoJoven.setText("0");
        jTextFieldCostoVariableVinoJoven.setName("jTextFieldPUVinoJoven"); // NOI18N
        getContentPane().add(jTextFieldCostoVariableVinoJoven, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 500, 93, -1));

        jTextFieldCostoVariableVinoEspumoso.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoEspumoso.setText("0");
        jTextFieldCostoVariableVinoEspumoso.setName("jTextFieldPUVinoEspumoso"); // NOI18N
        getContentPane().add(jTextFieldCostoVariableVinoEspumoso, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 490, 87, -1));

        jTextFieldCostoVariableVinoAnejo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoAnejo.setText("0");
        jTextFieldCostoVariableVinoAnejo.setName("jTextFieldPUVinoAnejo"); // NOI18N
        getContentPane().add(jTextFieldCostoVariableVinoAnejo, new org.netbeans.lib.awtextra.AbsoluteConstraints(871, 497, 93, -1));

        jLabel1.setFont(new java.awt.Font("Tempus Sans ITC", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 0, 102));
        jLabel1.setText("Producción de Vinos");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 20, -1, -1));

        jButtonSoluciones.setBackground(new java.awt.Color(0, 0, 255));
        jButtonSoluciones.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSoluciones.setText("Soluciones");
        jButtonSoluciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolucionesActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonSoluciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 530, 120, 30));

        jLabelTerreno.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelTerreno.setText("Terreno");
        getContentPane().add(jLabelTerreno, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 70, 20));

        jLabelFondo1.setIcon(new javax.swing.ImageIcon("C:\\Users\\mramoss\\Documents\\java\\Simulacion\\Vinos\\src\\Imag\\uva.png")); // NOI18N
        getContentPane().add(jLabelFondo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, 70, 70));

        jLabelCVVinoAnejo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCVVinoAnejo.setText("C. V. Vino Añejo ($)");
        getContentPane().add(jLabelCVVinoAnejo, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 500, -1, -1));

        jLabelPUVinoJoven1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoJoven1.setText("P/U Vino Joven ($)");
        getContentPane().add(jLabelPUVinoJoven1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 460, -1, -1));

        jLabelPUVinoEspumoso1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoEspumoso1.setText("P/U Vino  Espumoso ($)");
        getContentPane().add(jLabelPUVinoEspumoso1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 460, -1, -1));

        jLabelCostoFijo2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCostoFijo2.setText("Costo Fijo:");
        getContentPane().add(jLabelCostoFijo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(39, 462, -1, -1));

        jLabelCVVinoEspumoso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCVVinoEspumoso.setText("C. V. Vino Espumoso ($)");
        getContentPane().add(jLabelCVVinoEspumoso, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 500, -1, -1));

        jLabelFondo67.setIcon(new javax.swing.ImageIcon("C:\\Users\\mramoss\\Documents\\java\\Simulacion\\Vinos\\src\\Imag\\vino64.png")); // NOI18N
        getContentPane().add(jLabelFondo67, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 10, 70, 70));

        jMenu6.setText("Opciones");

        jMenuMixto.setText("Generador Mixto");
        jMenuMixto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuMixtoMouseClicked(evt);
            }
        });
        jMenuMixto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuMixtoActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuMixto);

        jMenuBar2.add(jMenu6);

        jMenu7.setText("Salir");
        jMenuBar2.add(jMenu7);

        setJMenuBar(jMenuBar2);

        getAccessibleContext().setAccessibleName("frameSimulacionProduccionVinos");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldTerrenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTerrenoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTerrenoActionPerformed

    private void jTextFieldCostoFijoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCostoFijoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCostoFijoActionPerformed

    private void jButtonPorDefectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPorDefectoActionPerformed
        jTextFieldTerreno.setText("3");
        jTextFieldProdHectaria.setText("9000");
        jTextFieldCostoFijo.setText("15600");
        jTextFieldPUVinoEspumoso.setText("120");
        jTextFieldPUVinoJoven.setText("95");
        jTextFieldPUVinoAnejo.setText("450");
        jTextFieldCostoVariableVinoEspumoso.setText("3.10");
        jTextFieldCostoVariableVinoJoven.setText("2.10");
        jTextFieldCostoVariableVinoAnejo.setText("7.10");

    }//GEN-LAST:event_jButtonPorDefectoActionPerformed

    private void jButtonSimularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSimularActionPerformed
        // agregarDatosTablaCosecha();
        try {
            String terreno = jTextFieldTerreno.getText();
            String prodHectaria = jTextFieldProdHectaria.getText();
            String costoFijo = jTextFieldCostoFijo.getText();
            String pUVinoEspumoso = jTextFieldPUVinoEspumoso.getText();
            String pUVinoJove = jTextFieldPUVinoJoven.getText();
            String pUVinoAnejo = jTextFieldPUVinoAnejo.getText();
            String costoVariableVinoEspumoso = jTextFieldCostoVariableVinoEspumoso.getText();
            String costoVariableVinoJoven = jTextFieldCostoVariableVinoJoven.getText();
            String costoVariableVinoAnejo = jTextFieldCostoVariableVinoAnejo.getText();
            if (!terreno.isEmpty() && !prodHectaria.isEmpty() && !pUVinoEspumoso.isEmpty() && !pUVinoJove.isEmpty()
                    && !pUVinoAnejo.isEmpty() && !costoVariableVinoEspumoso.isEmpty() && !costoVariableVinoJoven.isEmpty()
                    && !costoVariableVinoAnejo.isEmpty() && !costoFijo.isEmpty()) {
                agregarDatosTablaCosecha();
            } else {
                JOptionPane.showMessageDialog(null, "Llenar con datos los campos");
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Debe ingresar Nros");
            limpiarCampos();
        }
    }//GEN-LAST:event_jButtonSimularActionPerformed
    private void limpiarCampos() {
        jTextFieldTerreno.setText("");
        jTextFieldProdHectaria.setText("");
        jTextFieldCostoFijo.setText("");
        jTextFieldPUVinoEspumoso.setText("");
        jTextFieldPUVinoJoven.setText("");
        jTextFieldPUVinoAnejo.setText("");
        jTextFieldCostoVariableVinoEspumoso.setText("");
        jTextFieldCostoVariableVinoJoven.setText("");
        jTextFieldCostoVariableVinoAnejo.setText("");

        eliminarDatosTabla(tablaCosecha);
        eliminarDatosTabla(tablaMacFerMad);
        eliminarDatosTabla(tablaCostoUtilidad);
    }
    private void jButtonVaciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVaciarActionPerformed
        // TODO add your handling code here:
        limpiarCampos();
    }//GEN-LAST:event_jButtonVaciarActionPerformed

    private void jMenuMixtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuMixtoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuMixtoActionPerformed

    private void jMenuMixtoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuMixtoMouseClicked
        // TODO add your handling code
        GeneradorMixto newMixto = new GeneradorMixto();
        newMixto.setVisible(true);

    }//GEN-LAST:event_jMenuMixtoMouseClicked

    private void jButtonSolucionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolucionesActionPerformed
        // TODO add your handling code here:

        if (tablaCostoUtilidad.getRowCount() > 0) {
            Resultados newResultados = new Resultados();

            String prodEspumantes = String.valueOf(tablaCostoUtilidad.getValueAt(0, 1));
            double valorProdEspumantes = Double.parseDouble(prodEspumantes.replace(",", "."));
            String prodJovenes = String.valueOf(tablaCostoUtilidad.getValueAt(1, 1));
            double valorProJovenes = Double.parseDouble(prodJovenes.replace(",", "."));
            String prodAnejos = String.valueOf(tablaCostoUtilidad.getValueAt(2, 1));
            double valorProdAnejos = Double.parseDouble(prodAnejos.replace(",", "."));

            newResultados.jTextFieldProdEspumante.setText(prodEspumantes);
            newResultados.jTextFieldProdJoven.setText(prodJovenes);
            newResultados.jTextFieldProdAnejos.setText(prodAnejos);

            newResultados.jTextFieldCostoFijo.setText(jTextFieldCostoFijo.getText());

            newResultados.jTextFieldPUVinoEspumoso.setText(jTextFieldPUVinoEspumoso.getText());
            newResultados.jTextFieldPUVinoJoven.setText(jTextFieldPUVinoJoven.getText());
            newResultados.jTextFieldPUVinoAnejo.setText(jTextFieldPUVinoAnejo.getText());

            newResultados.jTextFieldCostoVariableVinoEspumoso.setText(jTextFieldCostoVariableVinoEspumoso.getText());
            newResultados.jTextFieldCostoVariableVinoJoven.setText(jTextFieldCostoVariableVinoJoven.getText());
            newResultados.jTextFieldCostoVariableVinoAnejo.setText(jTextFieldCostoVariableVinoAnejo.getText());

            newResultados.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Debe hacer una Simulación Primero..!!");
        }

    }//GEN-LAST:event_jButtonSolucionesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonPorDefecto;
    private javax.swing.JButton jButtonSimular;
    private javax.swing.JButton jButtonSoluciones;
    private javax.swing.JButton jButtonVaciar;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelCVVinoAnejo;
    private javax.swing.JLabel jLabelCVVinoEspumoso;
    private javax.swing.JLabel jLabelCostoFijo2;
    private javax.swing.JLabel jLabelFondo1;
    private javax.swing.JLabel jLabelFondo67;
    private javax.swing.JLabel jLabelPUVinoEspumoso;
    private javax.swing.JLabel jLabelPUVinoEspumoso1;
    private javax.swing.JLabel jLabelPUVinoJoven1;
    private javax.swing.JLabel jLabelProdHectaria;
    private javax.swing.JLabel jLabelTerreno;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenu jMenuMixto;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTableCosecha;
    private javax.swing.JTable jTableCostoUtilidad;
    private javax.swing.JTable jTableMacFerMad;
    private javax.swing.JTextField jTextFieldCostoFijo;
    private javax.swing.JTextField jTextFieldCostoVariableVinoAnejo;
    private javax.swing.JTextField jTextFieldCostoVariableVinoEspumoso;
    private javax.swing.JTextField jTextFieldCostoVariableVinoJoven;
    private javax.swing.JTextField jTextFieldPUVinoAnejo;
    private javax.swing.JTextField jTextFieldPUVinoEspumoso;
    private javax.swing.JTextField jTextFieldPUVinoJoven;
    private javax.swing.JTextField jTextFieldProdHectaria;
    private javax.swing.JTextField jTextFieldTerreno;
    private java.awt.Label labelCVVinoJoven;
    private java.awt.Label labelConstantes;
    private java.awt.Label labelMacFerMad;
    private java.awt.Label labelMacFerMad1;
    private java.awt.Panel panel1;
    // End of variables declaration//GEN-END:variables
}
