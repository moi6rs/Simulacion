/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import generador.CongruencialMixto;
import generador.DistribucionSimulacion;
import graficacion.Barras;
import graficacion.Torta;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.table.DefaultTableModel;
import produccionVinos.OpcionesSolucion;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author mramoss
 */
public class Resultados extends javax.swing.JFrame {

    /**
     * Creates new form Resultados
     */
    DefaultTableModel tablaOpcionUno = new DefaultTableModel();
    DefaultTableModel tablaOpcionDos = new DefaultTableModel();
    DefaultTableModel tablaOpcionTres = new DefaultTableModel();
    public ArrayList<Double> nrosAleatorios = new ArrayList<>();

    public Resultados() {
        initComponents();
        ejecutarTablaOpcionUno();
        ejecutarTablaOpcionDos();
        ejecutarTablaOpcionTres();
        setNrosAleatorios();
    }

    public void setNrosAleatorios() {
        ArrayList<Double> nrosAle = new ArrayList<>();
        CongruencialMixto newCongruencialMixto = new CongruencialMixto(61.0, 11.0, 421.0, 1000.0, 11.0);
        this.nrosAleatorios = newCongruencialMixto.generar();
    }

    private void ejecutarTablaOpcionUno() {
        String[] cabecera = new String[]{"Producto", "Demanda",
            "Prod. Vino", "Prod. a Satisfacer", "Diferencia",
            "Dem. Satisfecha", "Utilidad con Ajuste"
        };
        tablaOpcionUno.setColumnIdentifiers(cabecera);
        jTableOpcion1.setModel(tablaOpcionUno);
    }

    private void ejecutarTablaOpcionDos() {
        String[] cabecera = new String[]{"Producto", "Demanda",
            "Prod. Vino", "Prod. a Satisfacer", "Diferencia",
            "Dem. Satisfecha", "Utilidad con Ajuste"
        };
        tablaOpcionDos.setColumnIdentifiers(cabecera);
        jTableOpcion2.setModel(tablaOpcionDos);
    }

    private void ejecutarTablaOpcionTres() {
        String[] cabecera = new String[]{"Producto", "Demanda",
            "Prod. Vino", "Prod. a Satisfacer", "Diferencia",
            "Dem. Satisfecha", "Utilidad con Ajuste"
        };
        tablaOpcionTres.setColumnIdentifiers(cabecera);
        jTableOpcion3.setModel(tablaOpcionTres);
    }

    private void cargarTablaOpcionUno() {
        ArrayList<String> tipoVinos = new ArrayList<>();
        tipoVinos.add("Espumantes");
        tipoVinos.add("Jovenes");
        tipoVinos.add("Añejados en Roble");

        ArrayList<Double> demandaVinos = demandaGeneradaVinos();
        ArrayList<Double> prodVinoReal = produccionVinoReal();
        ArrayList<Double> precioVinos = preciosVentaVinos();
        ArrayList<Double> costoVinos = costosVariablesVinos();
        String nombreVino = "";
        double demandaVino = 0.0;
        double prodVino = 0.0;
        double prodSatisfacer = 0.0;
        double diferencia = 0.0;
        double demSatisfacer = 0.0;
        double costoVinoUd = 0.0;
        double precioVinoUd = 0.0;
        String costoFijo = jTextFieldCostoFijo.getText();
        double costoFijoVinos = Double.parseDouble(costoFijo);
        double utilidad = 0.0;
        double sumaTotal = 0.0;

        for (int i = 0; i < tipoVinos.size(); i++) {
            nombreVino = tipoVinos.get(i);
            demandaVino = demandaVinos.get(i);
            prodVino = prodVinoReal.get(i);
            costoVinoUd = costoVinos.get(i);
            precioVinoUd = precioVinos.get(i);
            demSatisfacer = demandaVino;

            if (tipoVinos.get(i) == "Espumantes") {
                diferencia = prodVino - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionUno(nombreVino, demandaVino, prodVino, prodVino, diferencia, demSatisfacer, utilidad);
            }
            if (tipoVinos.get(i) == "Jovenes") {
                prodSatisfacer = prodVino - demandaVinos.get(2);
                diferencia = prodSatisfacer - demandaVino;
                demSatisfacer = prodSatisfacer;
                utilidad = (prodSatisfacer * precioVinoUd) - ((prodSatisfacer * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionUno(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);

            }
            if (tipoVinos.get(i) == "Añejados en Roble") {
                prodSatisfacer = demandaVino;
                diferencia = prodSatisfacer - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionUno(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);
            }
        }
    }

    private void cargarTablaOpcionDos() {
        ArrayList<String> tipoVinos = new ArrayList<>();
        tipoVinos.add("Espumantes");
        tipoVinos.add("Jovenes");
        tipoVinos.add("Añejados en Roble");

        ArrayList<Double> demandaVinos = demandaGeneradaVinos();
        ArrayList<Double> prodVinoReal = produccionVinoReal();
        ArrayList<Double> precioVinos = preciosVentaVinos();
        ArrayList<Double> costoVinos = costosVariablesVinos();
        String nombreVino = "";
        double demandaVino = 0.0;
        double prodVino = 0.0;
        double prodSatisfacer = 0.0;
        double diferencia = 0.0;
        double demSatisfacer = 0.0;
        double costoVinoUd = 0.0;
        double precioVinoUd = 0.0;
        String costoFijo = jTextFieldCostoFijo.getText();
        double costoFijoVinos = Double.parseDouble(costoFijo);
        double utilidad = 0.0;
        double sumaTotal = 0.0;

        for (int i = 0; i < tipoVinos.size(); i++) {
            nombreVino = tipoVinos.get(i);
            demandaVino = demandaVinos.get(i);
            prodVino = prodVinoReal.get(i);
            costoVinoUd = costoVinos.get(i);
            precioVinoUd = precioVinos.get(i);
            demSatisfacer = demandaVino;

            if (tipoVinos.get(i) == "Espumantes") {
                diferencia = prodVino - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionDos(nombreVino, demandaVino, prodVino, prodVino, diferencia, demSatisfacer, utilidad);
            }
            if (tipoVinos.get(i) == "Jovenes") {
                prodSatisfacer = demandaVino;
                diferencia = prodSatisfacer - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionDos(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);
            }
            if (tipoVinos.get(i) == "Añejados en Roble") {
                prodSatisfacer = prodVino - demandaVinos.get(1);
                diferencia = prodSatisfacer - demandaVino;
                demSatisfacer = prodSatisfacer;
                utilidad = (prodSatisfacer * precioVinoUd) - ((prodSatisfacer * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionDos(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);
            }
        }
    }

    private void agregarDatosTablaOpcionUno(String nombreVino, double demandaVino, double prodVino, double prodSatisfacer,
            double diferencia, double demSatisfacer, double utilidad) {
        OpcionesSolucion opcionSolucion = new OpcionesSolucion(nombreVino, demandaVino, prodVino);
        tablaOpcionUno.addRow(new Object[]{
            opcionSolucion.getNombreVino(),
            redondear(opcionSolucion.getDemandaVino()),
            redondear(opcionSolucion.getProducionVino()),
            redondear(prodSatisfacer),
            redondear(diferencia),
            redondear(demSatisfacer),
            redondear(utilidad)
        });
    }

    private void agregarDatosTablaOpcionDos(String nombreVino, double demandaVino, double prodVino, double prodSatisfacer,
            double diferencia, double demSatisfacer, double utilidad) {
        OpcionesSolucion opcionSolucion = new OpcionesSolucion(nombreVino, demandaVino, prodVino);
        tablaOpcionDos.addRow(new Object[]{
            opcionSolucion.getNombreVino(),
            redondear(opcionSolucion.getDemandaVino()),
            redondear(opcionSolucion.getProducionVino()),
            redondear(prodSatisfacer),
            redondear(diferencia),
            redondear(demSatisfacer),
            redondear(utilidad)
        });
    }

    private void cargarTablaOpcionTres() {
        ArrayList<String> tipoVinos = new ArrayList<>();
        tipoVinos.add("Espumantes");
        tipoVinos.add("Jovenes");
        tipoVinos.add("Añejados en Roble");

        ArrayList<Double> demandaVinos = demandaGeneradaVinos();
        ArrayList<Double> prodVinoReal = produccionVinoReal();
        ArrayList<Double> precioVinos = preciosVentaVinos();
        ArrayList<Double> costoVinos = costosVariablesVinos();
        String nombreVino = "";
        double demandaVino = 0.0;
        double prodVino = 0.0;
        double prodSatisfacer = 0.0;
        double diferencia = 0.0;
        double demSatisfacer = 0.0;
        double costoVinoUd = 0.0;
        double precioVinoUd = 0.0;
        String costoFijo = jTextFieldCostoFijo.getText();
        double costoFijoVinos = Double.parseDouble(costoFijo);
        double utilidad = 0.0;
        double sumaTotal = 0.0;

        for (int i = 0; i < tipoVinos.size(); i++) {
            nombreVino = tipoVinos.get(i);
            demandaVino = demandaVinos.get(i);
            prodVino = prodVinoReal.get(i);
            costoVinoUd = costoVinos.get(i);
            precioVinoUd = precioVinos.get(i);
            demSatisfacer = demandaVino;

            if (tipoVinos.get(i) == "Espumantes") {
                diferencia = prodVino - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionTres(nombreVino, demandaVino, prodVino, prodVino, diferencia, demSatisfacer, utilidad);
            }
            if (tipoVinos.get(i) == "Jovenes") {
                prodSatisfacer = demandaVino;
                diferencia = prodSatisfacer - demandaVino;
                utilidad = (demandaVino * precioVinoUd) - ((demandaVino * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionTres(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);
            }
            if (tipoVinos.get(i) == "Añejados en Roble") {
                prodSatisfacer = 0.0;
                diferencia = prodSatisfacer - demandaVino;
                demSatisfacer = prodSatisfacer;
                utilidad = (prodSatisfacer * precioVinoUd) - ((prodSatisfacer * costoVinoUd) + costoFijoVinos);
                agregarDatosTablaOpcionTres(nombreVino, demandaVino, prodVino, prodSatisfacer, diferencia, demSatisfacer, utilidad);
            }
        }
    }

    private void agregarDatosTablaOpcionTres(String nombreVino, double demandaVino, double prodVino, double prodSatisfacer,
            double diferencia, double demSatisfacer, double utilidad) {
        OpcionesSolucion opcionSolucion = new OpcionesSolucion(nombreVino, demandaVino, prodVino);
        tablaOpcionTres.addRow(new Object[]{
            opcionSolucion.getNombreVino(),
            redondear(opcionSolucion.getDemandaVino()),
            redondear(opcionSolucion.getProducionVino()),
            redondear(prodSatisfacer),
            redondear(diferencia),
            redondear(demSatisfacer),
            redondear(utilidad)
        });
    }

    private ArrayList<Double> preciosVentaVinos() {
        ArrayList<Double> preciosVenta = new ArrayList<>();

        String puEspumoso = jTextFieldPUVinoEspumoso.getText();
        double precioUdEspumoso = Double.parseDouble(puEspumoso);
        String puJoven = jTextFieldPUVinoJoven.getText();
        double precioUdJoven = Double.parseDouble(puJoven);
        String puAnejo = jTextFieldPUVinoAnejo.getText();
        double precioUdAnejo = Double.parseDouble(puAnejo);

        preciosVenta.add(precioUdEspumoso);
        preciosVenta.add(precioUdJoven);
        preciosVenta.add(precioUdAnejo);
        return preciosVenta;
    }

    private ArrayList<Double> costosVariablesVinos() {
        ArrayList<Double> costosVariable = new ArrayList<>();
        String cvEspumoso = jTextFieldCostoVariableVinoEspumoso.getText();
        double costoVarEspumoso = Double.parseDouble(cvEspumoso.replace(",", "."));
        String cvJoven = jTextFieldCostoVariableVinoJoven.getText();
        double costoVarJoven = Double.parseDouble(cvJoven.replace(",", "."));
        String cvAnejo = jTextFieldCostoVariableVinoAnejo.getText();
        double costoVarAnejo = Double.parseDouble(cvAnejo.replace(",", "."));

        costosVariable.add(costoVarEspumoso);
        costosVariable.add(costoVarJoven);
        costosVariable.add(costoVarAnejo);
        return costosVariable;
    }

    private ArrayList<Double> demandaGeneradaVinos() {
        ArrayList<Double> demandaVinoGenerada = new ArrayList<>();

        String demEspumoso = jTextFieldDemEspumante.getText();
        Double demandaEspumoso = Double.parseDouble(demEspumoso.replace(",", "."));
        String demJoven = jTextFieldDemJoven.getText();
        Double demandaJoven = Double.parseDouble(demJoven.replace(",", "."));
        String demAnejo = jTextFieldDemAnejos.getText();
        Double demandAnejo = Double.parseDouble(demAnejo.replace(",", "."));

        demandaVinoGenerada.add(demandaEspumoso);
        demandaVinoGenerada.add(demandaJoven);
        demandaVinoGenerada.add(demandAnejo);
        return demandaVinoGenerada;
    }

    private ArrayList<Double> produccionVinoReal() {
        ArrayList<Double> demandaVinoGenerada = new ArrayList<>();

        String demEspumoso = jTextFieldProdEspumante.getText();
        double demandaEspumoso = Double.parseDouble(demEspumoso.replace(",", "."));
        String demJoven = jTextFieldProdJoven.getText();
        double demandaJoven = Double.parseDouble(demJoven.replace(",", "."));
        String demAnejo = jTextFieldProdAnejos.getText();
        double demandAnejo = Double.parseDouble(demAnejo.replace(",", "."));

        demandaVinoGenerada.add(demandaEspumoso);
        demandaVinoGenerada.add(demandaJoven);
        demandaVinoGenerada.add(demandAnejo);
        return demandaVinoGenerada;
    }

    private String redondear(double nro) {
        String nroRedondeado = " ";
        DecimalFormat formato1 = new DecimalFormat("0.00");
        nroRedondeado = formato1.format(nro);
        return nroRedondeado;
    }

    private ArrayList<Double> litrosVino() {
        ArrayList<Double> m3Vino = new ArrayList();
        String metros3EspumosoUno = jTextFieldM3Una.getText();
        double litrosEspumosoUno = Double.parseDouble(metros3EspumosoUno);
        String metros3EspumosoDos = jTextFieldM3Dos.getText();
        double litrosEspumosoDos = Double.parseDouble(metros3EspumosoDos);
        String metros3EspumosoTres = jTextFieldM3Tres.getText();
        double litrosEspumosoTres = Double.parseDouble(metros3EspumosoTres);
        m3Vino.add(litrosEspumosoUno);
        m3Vino.add(litrosEspumosoDos);
        m3Vino.add(litrosEspumosoTres);
        return m3Vino;
    }

    private ArrayList<Double> probabilidadVino() {
        ArrayList<Double> probVino = new ArrayList();
        String probEspUno = jTextFieldProbUno.getText();
        double probEspumosoUno = Double.parseDouble(probEspUno);
        String probEspDos = jTextFieldProbDos.getText();
        double probEspumosoDos = Double.parseDouble(probEspDos);
        String probEspTres = jTextFieldProbTres.getText();
        double probEspumosoTres = Double.parseDouble(probEspTres);
        probVino.add(probEspumosoUno);
        probVino.add(probEspumosoDos);
        probVino.add(probEspumosoTres);
        return probVino;
    }

    private void eliminarDatosTabla(DefaultTableModel tablaDatos) {
        if (tablaDatos.getRowCount() > 0) {
            int filas = tablaDatos.getRowCount();
            for (int i = 0; i < filas; i++) {
                tablaDatos.removeRow(i);
                i -= 1;
            }
            System.out.println("Datos Eliminados de Tabla..");
        } else {
            System.out.println("La tabla ya esta vacia..!!");
        }
    }

    private void vaciarCamposResultado() {
        jTextFieldMinJovenes.setText("");
        jTextFieldPromedioJovenes.setText("");
        jTextFieldMaxJovenes.setText("");
        jTextFieldMinAnejos.setText("");
        jTextFieldPromedioAnejos.setText("");
        jTextFieldMaxAnejos.setText("");
        jTextFieldM3Una.setText("");
        jTextFieldM3Dos.setText("");
        jTextFieldM3Tres.setText("");
        jTextFieldProbUno.setText("");
        jTextFieldProbDos.setText("");
        jTextFieldProbTres.setText("");
        jTextFieldDemJoven.setText("");
        jTextFieldDemAnejos.setText("");
        jTextFieldDemEspumante.setText("");
        eliminarDatosTabla(tablaOpcionUno);
        eliminarDatosTabla(tablaOpcionDos);
        eliminarDatosTabla(tablaOpcionTres);
    }

    private void cargarTotalesUno() {
        double suma = 0.0;
        if (tablaOpcionUno.getRowCount() > 0) {
            String utilidadEspumantes = String.valueOf(tablaOpcionUno.getValueAt(0, 6));
            double valorEspumantes = Double.parseDouble(utilidadEspumantes.replace(",", "."));
            String utilidadJovenes = String.valueOf(tablaOpcionUno.getValueAt(1, 6));
            double valorJovenes = Double.parseDouble(utilidadJovenes.replace(",", "."));
            String utilidadAnejos = String.valueOf(tablaOpcionUno.getValueAt(2, 6));
            double valorAnejos = Double.parseDouble(utilidadAnejos.replace(",", "."));
            suma = valorAnejos + valorEspumantes + valorJovenes;
        } else {
            JOptionPane.showMessageDialog(null, "Debe hacer una Simulación Primero..!!");
        }
        jTextFieldTotalOpcionUno.setText(redondear(suma));
    }

    private void cargarTotalesDos() {
        double suma = 0.0;
        if (tablaOpcionDos.getRowCount() > 0) {
            String utilidadEspumantes = String.valueOf(tablaOpcionDos.getValueAt(0, 6));
            double valorEspumantes = Double.parseDouble(utilidadEspumantes.replace(",", "."));
            String utilidadJovenes = String.valueOf(tablaOpcionDos.getValueAt(1, 6));
            double valorJovenes = Double.parseDouble(utilidadJovenes.replace(",", "."));
            String utilidadAnejos = String.valueOf(tablaOpcionDos.getValueAt(2, 6));
            double valorAnejos = Double.parseDouble(utilidadAnejos.replace(",", "."));
            suma = valorAnejos + valorEspumantes + valorJovenes;
        } else {
            JOptionPane.showMessageDialog(null, "Debe hacer una Simulación Primero..!!");
        }
        jTextFieldTotalOpcionDos.setText(redondear(suma));
    }

    private void cargarTotalesTres() {
        double suma = 0.0;
        if (tablaOpcionTres.getRowCount() > 0) {
            String utilidadEspumantes = String.valueOf(tablaOpcionTres.getValueAt(0, 6));
            double valorEspumantes = Double.parseDouble(utilidadEspumantes.replace(",", "."));
            String utilidadJovenes = String.valueOf(tablaOpcionTres.getValueAt(1, 6));
            double valorJovenes = Double.parseDouble(utilidadJovenes.replace(",", "."));
            String utilidadAnejos = String.valueOf(tablaOpcionTres.getValueAt(2, 6));
            double valorAnejos = Double.parseDouble(utilidadAnejos.replace(",", "."));
            suma = valorAnejos + valorEspumantes + valorJovenes;
        } else {
            JOptionPane.showMessageDialog(null, "Debe hacer una Simulación Primero..!!");
        }
        jTextFieldTotalOpcionTres.setText(redondear(suma));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabelMinBotellasJovenes = new javax.swing.JLabel();
        jLabelPromedioBotellasJovenes = new javax.swing.JLabel();
        jLabelMaxBotellasJovenes2 = new javax.swing.JLabel();
        jTextFieldMinJovenes = new javax.swing.JTextField();
        jTextFieldPromedioJovenes = new javax.swing.JTextField();
        jTextFieldMaxJovenes = new javax.swing.JTextField();
        jLabelMinBotellasAnejos = new javax.swing.JLabel();
        jLabelPromedioBotellasAnejos = new javax.swing.JLabel();
        jLabelMaxBotellasAnejos = new javax.swing.JLabel();
        jTextFieldMinAnejos = new javax.swing.JTextField();
        jTextFieldPromedioAnejos = new javax.swing.JTextField();
        jTextFieldMaxAnejos = new javax.swing.JTextField();
        jLabelMinBotellasJovenes1 = new javax.swing.JLabel();
        jLabelPromedioBotellasJovenes1 = new javax.swing.JLabel();
        jTextFieldM3Una = new javax.swing.JTextField();
        jTextFieldProbUno = new javax.swing.JTextField();
        jTextFieldM3Dos = new javax.swing.JTextField();
        jTextFieldProbDos = new javax.swing.JTextField();
        jTextFieldM3Tres = new javax.swing.JTextField();
        jTextFieldProbTres = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableOpcion1 = new javax.swing.JTable();
        jButtonPorDefecto = new javax.swing.JButton();
        jButtonVaciar = new javax.swing.JButton();
        jButtonSimular = new javax.swing.JButton();
        labelDemandaJovenes = new java.awt.Label();
        labelOpcion1 = new java.awt.Label();
        labelDemandaAnejos = new java.awt.Label();
        labelFunciones = new java.awt.Label();
        lavelResultado = new java.awt.Label();
        labelDemandaEspumante4 = new java.awt.Label();
        jLabelDemEspumantes = new javax.swing.JLabel();
        jTextFieldDemEspumante = new javax.swing.JTextField();
        jLabelDemandaEspumantes = new javax.swing.JLabel();
        jTextFieldDemJoven = new javax.swing.JTextField();
        jLabelDemAnejos = new javax.swing.JLabel();
        jTextFieldDemAnejos = new javax.swing.JTextField();
        jLabelPUVinoEspumoso = new javax.swing.JLabel();
        jTextFieldCostoFijo = new javax.swing.JTextField();
        jTextFieldPUVinoJoven = new javax.swing.JTextField();
        jTextFieldPUVinoEspumoso = new javax.swing.JTextField();
        jTextFieldPUVinoAnejo = new javax.swing.JTextField();
        labelCVVinoJoven = new java.awt.Label();
        jTextFieldCostoVariableVinoJoven = new javax.swing.JTextField();
        jTextFieldCostoVariableVinoEspumoso = new javax.swing.JTextField();
        jTextFieldCostoVariableVinoAnejo = new javax.swing.JTextField();
        jLabelCVVinoAnejo = new javax.swing.JLabel();
        jLabelPUVinoJoven1 = new javax.swing.JLabel();
        jLabelPUVinoEspumoso1 = new javax.swing.JLabel();
        jLabelCostoFijo2 = new javax.swing.JLabel();
        jLabelCVVinoEspumoso = new javax.swing.JLabel();
        lavelProduccion = new java.awt.Label();
        jLabelProdEspumantes = new javax.swing.JLabel();
        jTextFieldProdEspumante = new javax.swing.JTextField();
        jLabelDemandaEspumantes1 = new javax.swing.JLabel();
        jTextFieldProdJoven = new javax.swing.JTextField();
        jLabelProdAnejos = new javax.swing.JLabel();
        jTextFieldProdAnejos = new javax.swing.JTextField();
        jButtonCargar1 = new javax.swing.JButton();
        jLabelFondo2 = new javax.swing.JLabel();
        jLabelFondo66 = new javax.swing.JLabel();
        labelOpcion2 = new java.awt.Label();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableOpcion2 = new javax.swing.JTable();
        labelOpcion3 = new java.awt.Label();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableOpcion3 = new javax.swing.JTable();
        lavelTotalResultado = new java.awt.Label();
        jLabelTotalEspumantes = new javax.swing.JLabel();
        jTextFieldTotalOpcionUno = new javax.swing.JTextField();
        jLabeTotalEspumantes = new javax.swing.JLabel();
        jTextFieldTotalOpcionDos = new javax.swing.JTextField();
        jLabelOpcionAnejos = new javax.swing.JLabel();
        jTextFieldTotalOpcionTres = new javax.swing.JTextField();
        jButtonBarra = new javax.swing.JButton();
        jButtonTorta = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Opciones Solucion de Vinos");
        setName("frameDemandOpciones"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tempus Sans ITC", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 0, 102));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Demanda de Vinos");

        jLabelMinBotellasJovenes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMinBotellasJovenes.setText("Minimo");

        jLabelPromedioBotellasJovenes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelPromedioBotellasJovenes.setText("Promedio");

        jLabelMaxBotellasJovenes2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMaxBotellasJovenes2.setText("Máximo");

        jTextFieldMinJovenes.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldMinJovenes.setText("0");
        jTextFieldMinJovenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMinJovenesActionPerformed(evt);
            }
        });

        jTextFieldPromedioJovenes.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPromedioJovenes.setText("0");
        jTextFieldPromedioJovenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPromedioJovenesActionPerformed(evt);
            }
        });

        jTextFieldMaxJovenes.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldMaxJovenes.setText("0");
        jTextFieldMaxJovenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMaxJovenesActionPerformed(evt);
            }
        });

        jLabelMinBotellasAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMinBotellasAnejos.setText("Minimo");

        jLabelPromedioBotellasAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelPromedioBotellasAnejos.setText("Promedio");

        jLabelMaxBotellasAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMaxBotellasAnejos.setText("Máximo");

        jTextFieldMinAnejos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldMinAnejos.setText("0");
        jTextFieldMinAnejos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMinAnejosActionPerformed(evt);
            }
        });

        jTextFieldPromedioAnejos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPromedioAnejos.setText("0");
        jTextFieldPromedioAnejos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPromedioAnejosActionPerformed(evt);
            }
        });

        jTextFieldMaxAnejos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldMaxAnejos.setText("0");
        jTextFieldMaxAnejos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMaxAnejosActionPerformed(evt);
            }
        });

        jLabelMinBotellasJovenes1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMinBotellasJovenes1.setText("m3 de vino");

        jLabelPromedioBotellasJovenes1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelPromedioBotellasJovenes1.setText("Probabilidad");

        jTextFieldM3Una.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldM3Una.setText("0");
        jTextFieldM3Una.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldM3UnaActionPerformed(evt);
            }
        });

        jTextFieldProbUno.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProbUno.setText("0");
        jTextFieldProbUno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProbUnoActionPerformed(evt);
            }
        });

        jTextFieldM3Dos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldM3Dos.setText("0");
        jTextFieldM3Dos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldM3DosActionPerformed(evt);
            }
        });

        jTextFieldProbDos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProbDos.setText("0");
        jTextFieldProbDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProbDosActionPerformed(evt);
            }
        });

        jTextFieldM3Tres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldM3Tres.setText("0");
        jTextFieldM3Tres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldM3TresActionPerformed(evt);
            }
        });

        jTextFieldProbTres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProbTres.setText("0");
        jTextFieldProbTres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProbTresActionPerformed(evt);
            }
        });

        jTableOpcion1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane2.setViewportView(jTableOpcion1);

        jButtonPorDefecto.setBackground(new java.awt.Color(0, 102, 0));
        jButtonPorDefecto.setForeground(new java.awt.Color(255, 255, 255));
        jButtonPorDefecto.setText("Por Defecto");
        jButtonPorDefecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPorDefectoActionPerformed(evt);
            }
        });

        jButtonVaciar.setBackground(new java.awt.Color(204, 0, 0));
        jButtonVaciar.setForeground(new java.awt.Color(255, 255, 255));
        jButtonVaciar.setText("Vaciar");
        jButtonVaciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVaciarActionPerformed(evt);
            }
        });

        jButtonSimular.setBackground(new java.awt.Color(102, 0, 153));
        jButtonSimular.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSimular.setText("Simular Opciones");
        jButtonSimular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSimularActionPerformed(evt);
            }
        });

        labelDemandaJovenes.setBackground(new java.awt.Color(102, 0, 102));
        labelDemandaJovenes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelDemandaJovenes.setForeground(new java.awt.Color(255, 255, 255));
        labelDemandaJovenes.setName("labelDemandaJovenes"); // NOI18N
        labelDemandaJovenes.setText("DEMANDA DE VINOS JOVENES");

        labelOpcion1.setBackground(new java.awt.Color(102, 0, 102));
        labelOpcion1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelOpcion1.setForeground(new java.awt.Color(255, 255, 255));
        labelOpcion1.setName("labelOpcionUno"); // NOI18N
        labelOpcion1.setText("OPCIONES UNO SATISFACER VINOS AÑEJOS");

        labelDemandaAnejos.setBackground(new java.awt.Color(102, 0, 102));
        labelDemandaAnejos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelDemandaAnejos.setForeground(new java.awt.Color(255, 255, 255));
        labelDemandaAnejos.setName("labelDemandaEspumante"); // NOI18N
        labelDemandaAnejos.setText("DEMANDA DE VINOS ANEJOS");

        labelFunciones.setBackground(new java.awt.Color(102, 0, 102));
        labelFunciones.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelFunciones.setForeground(new java.awt.Color(255, 255, 255));
        labelFunciones.setName("labelDemandaEspumante"); // NOI18N
        labelFunciones.setText("FUNCIONES");

        lavelResultado.setBackground(new java.awt.Color(102, 0, 102));
        lavelResultado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lavelResultado.setForeground(new java.awt.Color(255, 255, 255));
        lavelResultado.setName("lavelResultado"); // NOI18N
        lavelResultado.setText("RESULTADO DEMANDA");

        labelDemandaEspumante4.setBackground(new java.awt.Color(102, 0, 102));
        labelDemandaEspumante4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelDemandaEspumante4.setForeground(new java.awt.Color(255, 255, 255));
        labelDemandaEspumante4.setName("labelDemandaEspumante"); // NOI18N
        labelDemandaEspumante4.setText("DEMANDA DE VINOS ESPUMANTES");

        jLabelDemEspumantes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDemEspumantes.setText("Demanda Vino Espumantes :");

        jTextFieldDemEspumante.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldDemEspumante.setText("0");
        jTextFieldDemEspumante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDemEspumanteActionPerformed(evt);
            }
        });

        jLabelDemandaEspumantes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDemandaEspumantes.setText("Demanda Vino Joven :");

        jTextFieldDemJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldDemJoven.setText("0");
        jTextFieldDemJoven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDemJovenActionPerformed(evt);
            }
        });

        jLabelDemAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDemAnejos.setText("Demanda Vino Añejos :");

        jTextFieldDemAnejos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldDemAnejos.setText("0");
        jTextFieldDemAnejos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDemAnejosActionPerformed(evt);
            }
        });

        jLabelPUVinoEspumoso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoEspumoso.setText("P/U Vino  Añejo ($)");

        jTextFieldCostoFijo.setEditable(false);
        jTextFieldCostoFijo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoFijo.setText("0");
        jTextFieldCostoFijo.setName("jTextFieldCostoFijo"); // NOI18N
        jTextFieldCostoFijo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCostoFijoActionPerformed(evt);
            }
        });

        jTextFieldPUVinoJoven.setEditable(false);
        jTextFieldPUVinoJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoJoven.setText("0");
        jTextFieldPUVinoJoven.setName("jTextFieldPUVinoJoven"); // NOI18N

        jTextFieldPUVinoEspumoso.setEditable(false);
        jTextFieldPUVinoEspumoso.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoEspumoso.setText("0");
        jTextFieldPUVinoEspumoso.setName("jTextFieldPUVinoEspumoso"); // NOI18N

        jTextFieldPUVinoAnejo.setEditable(false);
        jTextFieldPUVinoAnejo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPUVinoAnejo.setText("0");
        jTextFieldPUVinoAnejo.setName("jTextFieldPUVinoAnejo"); // NOI18N

        labelCVVinoJoven.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        labelCVVinoJoven.setName("labelPUVinoJoven"); // NOI18N
        labelCVVinoJoven.setText("C. V. Vino Joven ($)");

        jTextFieldCostoVariableVinoJoven.setEditable(false);
        jTextFieldCostoVariableVinoJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoJoven.setText("0");
        jTextFieldCostoVariableVinoJoven.setName("jTextFieldPUVinoJoven"); // NOI18N

        jTextFieldCostoVariableVinoEspumoso.setEditable(false);
        jTextFieldCostoVariableVinoEspumoso.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoEspumoso.setText("0");
        jTextFieldCostoVariableVinoEspumoso.setName("jTextFieldPUVinoEspumoso"); // NOI18N

        jTextFieldCostoVariableVinoAnejo.setEditable(false);
        jTextFieldCostoVariableVinoAnejo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldCostoVariableVinoAnejo.setText("0");
        jTextFieldCostoVariableVinoAnejo.setName("jTextFieldPUVinoAnejo"); // NOI18N

        jLabelCVVinoAnejo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCVVinoAnejo.setText("C. V. Vino Añejo ($)");

        jLabelPUVinoJoven1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoJoven1.setText("P/U Vino Joven ($)");

        jLabelPUVinoEspumoso1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelPUVinoEspumoso1.setText("P/U Vino  Espumoso ($)");

        jLabelCostoFijo2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCostoFijo2.setText("Costo Fijo:");

        jLabelCVVinoEspumoso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelCVVinoEspumoso.setText("C. V. Vino Espumoso ($)");

        lavelProduccion.setBackground(new java.awt.Color(102, 0, 102));
        lavelProduccion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lavelProduccion.setForeground(new java.awt.Color(255, 255, 255));
        lavelProduccion.setName("lavelResultado"); // NOI18N
        lavelProduccion.setText("RESULTADO PRODUCCION");

        jLabelProdEspumantes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelProdEspumantes.setText("Produción Vino Espumantes :");

        jTextFieldProdEspumante.setEditable(false);
        jTextFieldProdEspumante.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProdEspumante.setText("0");
        jTextFieldProdEspumante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProdEspumanteActionPerformed(evt);
            }
        });

        jLabelDemandaEspumantes1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDemandaEspumantes1.setText("Producción Vino Joven :");

        jTextFieldProdJoven.setEditable(false);
        jTextFieldProdJoven.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProdJoven.setText("0");
        jTextFieldProdJoven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProdJovenActionPerformed(evt);
            }
        });

        jLabelProdAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelProdAnejos.setText("Producción Vino Añejos :");

        jTextFieldProdAnejos.setEditable(false);
        jTextFieldProdAnejos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldProdAnejos.setText("0");
        jTextFieldProdAnejos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProdAnejosActionPerformed(evt);
            }
        });

        jButtonCargar1.setBackground(new java.awt.Color(0, 0, 255));
        jButtonCargar1.setForeground(new java.awt.Color(255, 255, 255));
        jButtonCargar1.setText("Cargar Demandas");
        jButtonCargar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCargar1ActionPerformed(evt);
            }
        });

        jLabelFondo2.setIcon(new javax.swing.ImageIcon("C:\\Users\\mramoss\\Documents\\java\\Simulacion\\Vinos\\src\\Imag\\uva.png")); // NOI18N

        jLabelFondo66.setIcon(new javax.swing.ImageIcon("C:\\Users\\mramoss\\Documents\\java\\Simulacion\\Vinos\\src\\Imag\\vino64.png")); // NOI18N

        labelOpcion2.setBackground(new java.awt.Color(102, 0, 102));
        labelOpcion2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelOpcion2.setForeground(new java.awt.Color(255, 255, 255));
        labelOpcion2.setName("labelOpcionUno"); // NOI18N
        labelOpcion2.setText("OPCIONES DOS SATISFACER VINOS JOVENES");

        jTableOpcion2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane3.setViewportView(jTableOpcion2);

        labelOpcion3.setBackground(new java.awt.Color(102, 0, 102));
        labelOpcion3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelOpcion3.setForeground(new java.awt.Color(255, 255, 255));
        labelOpcion3.setName("labelOpcionUno"); // NOI18N
        labelOpcion3.setText("OPCION TRES SOLO PRODUCE VINO JOVEN");

        jTableOpcion3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane4.setViewportView(jTableOpcion3);

        lavelTotalResultado.setBackground(new java.awt.Color(102, 0, 102));
        lavelTotalResultado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lavelTotalResultado.setForeground(new java.awt.Color(255, 255, 255));
        lavelTotalResultado.setName("lavelResultado"); // NOI18N
        lavelTotalResultado.setText("RESULTADO UTILIDAD 3 OPCIONES");

        jLabelTotalEspumantes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelTotalEspumantes.setText("Utilidad Opcion Uno");

        jTextFieldTotalOpcionUno.setEditable(false);
        jTextFieldTotalOpcionUno.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTotalOpcionUno.setText("0");
        jTextFieldTotalOpcionUno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalOpcionUnoActionPerformed(evt);
            }
        });

        jLabeTotalEspumantes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabeTotalEspumantes.setText("Utilidad Opcion Dos");

        jTextFieldTotalOpcionDos.setEditable(false);
        jTextFieldTotalOpcionDos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTotalOpcionDos.setText("0");
        jTextFieldTotalOpcionDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalOpcionDosActionPerformed(evt);
            }
        });

        jLabelOpcionAnejos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelOpcionAnejos.setText("Utilidad Opcion Tres");

        jTextFieldTotalOpcionTres.setEditable(false);
        jTextFieldTotalOpcionTres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTotalOpcionTres.setText("0");
        jTextFieldTotalOpcionTres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalOpcionTresActionPerformed(evt);
            }
        });

        jButtonBarra.setBackground(new java.awt.Color(153, 0, 102));
        jButtonBarra.setForeground(new java.awt.Color(255, 255, 255));
        jButtonBarra.setText("Barras");
        jButtonBarra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBarraActionPerformed(evt);
            }
        });

        jButtonTorta.setBackground(new java.awt.Color(0, 153, 51));
        jButtonTorta.setForeground(new java.awt.Color(255, 255, 255));
        jButtonTorta.setText("Torta");
        jButtonTorta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTortaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(394, 394, 394)
                .addComponent(jLabelFondo2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelFondo66, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(364, 364, 364))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelOpcion1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabelMaxBotellasJovenes2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldMaxJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabelPromedioBotellasJovenes)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldPromedioJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabelMinBotellasJovenes)
                                        .addGap(35, 35, 35)
                                        .addComponent(jTextFieldMinJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(labelDemandaJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelPromedioBotellasJovenes1)
                                        .addComponent(jLabelMinBotellasJovenes1))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jTextFieldM3Una, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextFieldM3Dos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jTextFieldProbDos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextFieldProbTres, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldProbUno, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldM3Tres, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelDemandaEspumante4, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabelMaxBotellasAnejos)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldMaxAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabelPromedioBotellasAnejos)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldPromedioAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabelMinBotellasAnejos)
                                    .addGap(35, 35, 35)
                                    .addComponent(jTextFieldMinAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(labelDemandaAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButtonPorDefecto, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                                    .addComponent(jButtonVaciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButtonSimular, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButtonCargar1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButtonBarra, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(jButtonTorta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(labelFunciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(300, 300, 300)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabelDemAnejos)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldDemAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelDemandaEspumantes, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldDemJoven, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelDemEspumantes)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldDemEspumante, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(lavelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(36, 36, 36)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lavelProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(jLabelProdAnejos)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jTextFieldProdAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabelDemandaEspumantes1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jTextFieldProdJoven, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabelProdEspumantes)
                                            .addGap(18, 18, 18)
                                            .addComponent(jTextFieldProdEspumante, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lavelTotalResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabelOpcionAnejos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGap(18, 18, 18)
                                            .addComponent(jTextFieldTotalOpcionTres, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabeTotalEspumantes, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelTotalEspumantes, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextFieldTotalOpcionUno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextFieldTotalOpcionDos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelCostoFijo2)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldCostoFijo, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(36, 36, 36)
                                        .addComponent(jLabelPUVinoJoven1)
                                        .addGap(14, 14, 14)
                                        .addComponent(jTextFieldPUVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(191, 191, 191)
                                        .addComponent(labelCVVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldCostoVariableVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelPUVinoEspumoso1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextFieldPUVinoEspumoso, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(32, 32, 32)
                                        .addComponent(jLabelPUVinoEspumoso)
                                        .addGap(4, 4, 4)
                                        .addComponent(jTextFieldPUVinoAnejo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabelCVVinoEspumoso)
                                        .addGap(10, 10, 10)
                                        .addComponent(jTextFieldCostoVariableVinoEspumoso, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(33, 33, 33)
                                        .addComponent(jLabelCVVinoAnejo)
                                        .addGap(5, 5, 5)
                                        .addComponent(jTextFieldCostoVariableVinoAnejo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 8, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelOpcion2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelOpcion3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane4))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelFondo66, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addComponent(jLabelFondo2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelDemandaJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMinBotellasJovenes)
                            .addComponent(jTextFieldMinJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelPromedioBotellasJovenes)
                            .addComponent(jTextFieldPromedioJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldMaxJovenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelMaxBotellasJovenes2))
                        .addGap(18, 18, 18))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelFunciones, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(13, 13, 13)
                                        .addComponent(jButtonPorDefecto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonVaciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jButtonCargar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButtonBarra, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jButtonSimular, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButtonTorta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelDemandaAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelMinBotellasAnejos)
                                    .addComponent(jTextFieldMinAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelPromedioBotellasAnejos)
                                    .addComponent(jTextFieldPromedioAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextFieldMaxAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelMaxBotellasAnejos))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lavelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMinBotellasJovenes1)
                            .addComponent(jTextFieldM3Una, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldM3Dos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldM3Tres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelDemEspumantes)
                                    .addComponent(jTextFieldDemEspumante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldProbDos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldProbUno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelPromedioBotellasJovenes1)
                                        .addComponent(jTextFieldProbTres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelDemandaEspumantes)
                                        .addComponent(jTextFieldDemJoven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelDemAnejos)
                            .addComponent(jTextFieldDemAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(labelDemandaEspumante4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lavelProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelProdEspumantes)
                            .addComponent(jTextFieldProdEspumante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelDemandaEspumantes1)
                            .addComponent(jTextFieldProdJoven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelProdAnejos)
                            .addComponent(jTextFieldProdAnejos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lavelTotalResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldTotalOpcionUno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabelTotalEspumantes, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldTotalOpcionDos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabeTotalEspumantes))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelOpcionAnejos)
                            .addComponent(jTextFieldTotalOpcionTres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabelCostoFijo2))
                    .addComponent(jTextFieldCostoFijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabelPUVinoJoven1))
                    .addComponent(jTextFieldPUVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelPUVinoEspumoso1)
                            .addComponent(jTextFieldPUVinoEspumoso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabelPUVinoEspumoso))
                    .addComponent(jTextFieldPUVinoAnejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldCostoVariableVinoEspumoso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCostoVariableVinoAnejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelCVVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelCVVinoEspumoso)
                                .addComponent(jTextFieldCostoVariableVinoJoven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelCVVinoAnejo))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelOpcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelOpcion2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelOpcion3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35))
        );

        labelFunciones.getAccessibleContext().setAccessibleName("FU");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldMinJovenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMinJovenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMinJovenesActionPerformed

    private void jTextFieldPromedioJovenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPromedioJovenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPromedioJovenesActionPerformed

    private void jTextFieldMaxJovenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMaxJovenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMaxJovenesActionPerformed

    private void jTextFieldMinAnejosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMinAnejosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMinAnejosActionPerformed

    private void jTextFieldPromedioAnejosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPromedioAnejosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPromedioAnejosActionPerformed

    private void jTextFieldMaxAnejosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMaxAnejosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMaxAnejosActionPerformed

    private void jTextFieldM3UnaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldM3UnaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldM3UnaActionPerformed

    private void jTextFieldProbUnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProbUnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProbUnoActionPerformed

    private void jTextFieldM3DosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldM3DosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldM3DosActionPerformed

    private void jTextFieldProbDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProbDosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProbDosActionPerformed

    private void jTextFieldM3TresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldM3TresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldM3TresActionPerformed

    private void jTextFieldProbTresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProbTresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProbTresActionPerformed

    private void jButtonPorDefectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPorDefectoActionPerformed
        // TODO add your handling code here:
        jTextFieldMinJovenes.setText("9000.00");
        jTextFieldPromedioJovenes.setText("11000.00");
        jTextFieldMaxJovenes.setText("11800.00");
        jTextFieldMinAnejos.setText("2100.00");
        jTextFieldPromedioAnejos.setText("2500.00");
        jTextFieldMaxAnejos.setText("4100.00");
        jTextFieldM3Una.setText("0.70");
        jTextFieldM3Dos.setText("0.80");
        jTextFieldM3Tres.setText("0.90");
        jTextFieldProbUno.setText("0.20");
        jTextFieldProbDos.setText("0.50");
        jTextFieldProbTres.setText("0.30");
    }//GEN-LAST:event_jButtonPorDefectoActionPerformed

    private void jButtonVaciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVaciarActionPerformed
        // TODO add your handling code here:
        vaciarCamposResultado();

    }//GEN-LAST:event_jButtonVaciarActionPerformed

    private void jTextFieldDemEspumanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDemEspumanteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDemEspumanteActionPerformed

    private void jTextFieldDemJovenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDemJovenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDemJovenActionPerformed

    private void jTextFieldDemAnejosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDemAnejosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDemAnejosActionPerformed

    private void jTextFieldCostoFijoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCostoFijoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCostoFijoActionPerformed

    private void jTextFieldProdEspumanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProdEspumanteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProdEspumanteActionPerformed

    private void jTextFieldProdJovenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProdJovenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProdJovenActionPerformed

    private void jTextFieldProdAnejosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProdAnejosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProdAnejosActionPerformed

    private void jButtonSimularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSimularActionPerformed
        // TODO add your handling code here:
        try {
            String demUno = jTextFieldDemAnejos.getText();
            String demJoven = jTextFieldDemJoven.getText();
            String demEspumante = jTextFieldDemEspumante.getText();
            if (!demUno.isEmpty() && !demJoven.isEmpty() && !demEspumante.isEmpty()) {
                cargarTablaOpcionUno();
                cargarTablaOpcionDos();
                cargarTablaOpcionTres();
                cargarTotalesUno();
                cargarTotalesDos();
                cargarTotalesTres();

            } else {
                JOptionPane.showMessageDialog(null, "Llenar con datos los campos");
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Debe ingresar Nros");
            vaciarCamposResultado();
        }

    }//GEN-LAST:event_jButtonSimularActionPerformed

    private void jButtonCargar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCargar1ActionPerformed
        // TODO add your handling code here:
        try {
            String minJoven = jTextFieldMinJovenes.getText();
            String mediaJoven = jTextFieldPromedioJovenes.getText();
            String maxJoven = jTextFieldMaxJovenes.getText();
            if (!minJoven.isEmpty() && !mediaJoven.isEmpty() && !maxJoven.isEmpty()) {

                DistribucionSimulacion newDistribucion = new DistribucionSimulacion();
                //Generar demanda Vinos Joven con Dist, Triangular           
                double valorMinJoven = Double.parseDouble(minJoven);

                double valorMediaJoven = Double.parseDouble(mediaJoven);

                double valorMaxJoven = Double.parseDouble(maxJoven);
                int numeroUno = ThreadLocalRandom.current().nextInt(0, 998 + 1);
                int numeroTres = ThreadLocalRandom.current().nextInt(0, 998 + 1);
                int numeroDos = ThreadLocalRandom.current().nextInt(0, 998 + 1);
                int numeroCuatro = ThreadLocalRandom.current().nextInt(0, 998 + 1);
                int numeroCinco = ThreadLocalRandom.current().nextInt(0, 998 + 1);

                double variableXJoven = newDistribucion.distribucionTriangular(valorMaxJoven,
                        valorMediaJoven, valorMinJoven, nrosAleatorios.get(numeroUno), nrosAleatorios.get(numeroDos));
                jTextFieldDemJoven.setText(redondear(variableXJoven));

                //Generar demanda Añejos con Dist, Triangular
                String minAnejo = jTextFieldMinAnejos.getText();
                double valorMinAnejo = Double.parseDouble(minAnejo);
                String mediaAnejo = jTextFieldPromedioAnejos.getText();
                double valorMediaAnejo = Double.parseDouble(mediaAnejo);
                String maxAnejo = jTextFieldMaxAnejos.getText();
                double valorMaxAnejo = Double.parseDouble(maxAnejo);
                double variableXAnejo = newDistribucion.distribucionTriangular(valorMaxAnejo,
                        valorMediaAnejo, valorMinAnejo, nrosAleatorios.get(numeroTres), nrosAleatorios.get(numeroCuatro));
                jTextFieldDemAnejos.setText(redondear(variableXAnejo));

                //Generar demand de  espumantes monte carlo
                double variableXEspumante = newDistribucion.monteCarloDemandaVinos(nrosAleatorios.get(numeroCinco), litrosVino(), probabilidadVino());
                jTextFieldDemEspumante.setText(redondear(variableXEspumante * 1000));
            } else {
                JOptionPane.showMessageDialog(null, "Llenar con datos los campos");
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Debe ingresar Nros");
            vaciarCamposResultado();
        }
    }//GEN-LAST:event_jButtonCargar1ActionPerformed

    private void jTextFieldTotalOpcionUnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalOpcionUnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalOpcionUnoActionPerformed

    private void jTextFieldTotalOpcionDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalOpcionDosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalOpcionDosActionPerformed

    private void jTextFieldTotalOpcionTresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalOpcionTresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalOpcionTresActionPerformed

    private void jButtonBarraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBarraActionPerformed
        // TODO add your handling code here:
        String utilidadUno = jTextFieldTotalOpcionUno.getText();
        double valorUtilidadUno = Double.parseDouble(utilidadUno.replace(",", "."));
        String utilidadDos = jTextFieldTotalOpcionDos.getText();
        double valorUtilidadDos = Double.parseDouble(utilidadDos.replace(",", "."));
        String utilidadTres = jTextFieldTotalOpcionTres.getText();
        double valorUtilidadTres = Double.parseDouble(utilidadTres.replace(",", "."));

        if (valorUtilidadUno > 0 && valorUtilidadDos > 0 && valorUtilidadTres > 0) {
            Barras histograma = new Barras();
            histograma.graficar(valorUtilidadUno, valorUtilidadDos, valorUtilidadTres);
        } else {
            JOptionPane.showMessageDialog(null, "Debe Simular Primero..!!");
        }
    }//GEN-LAST:event_jButtonBarraActionPerformed

    private void jButtonTortaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTortaActionPerformed
        // TODO add your handling code here:
        String utilidadUno = jTextFieldTotalOpcionUno.getText();
        double valorUtilidadUno = Double.parseDouble(utilidadUno.replace(",", "."));
        String utilidadDos = jTextFieldTotalOpcionDos.getText();
        double valorUtilidadDos = Double.parseDouble(utilidadDos.replace(",", "."));
        String utilidadTres = jTextFieldTotalOpcionTres.getText();
        double valorUtilidadTres = Double.parseDouble(utilidadTres.replace(",", "."));
        if (valorUtilidadUno >0 && valorUtilidadDos  > 0 && valorUtilidadTres> 0) {
            Torta newTorta = new Torta();
            newTorta.graficar(valorUtilidadUno, valorUtilidadDos, valorUtilidadTres);
        } else {
            JOptionPane.showMessageDialog(null, "Debe hacer una Simulación Primero..!!");
        }
    }//GEN-LAST:event_jButtonTortaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Resultados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Resultados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Resultados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Resultados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Resultados().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBarra;
    private javax.swing.JButton jButtonCargar1;
    private javax.swing.JButton jButtonPorDefecto;
    private javax.swing.JButton jButtonSimular;
    private javax.swing.JButton jButtonTorta;
    private javax.swing.JButton jButtonVaciar;
    private javax.swing.JLabel jLabeTotalEspumantes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCVVinoAnejo;
    private javax.swing.JLabel jLabelCVVinoEspumoso;
    private javax.swing.JLabel jLabelCostoFijo2;
    private javax.swing.JLabel jLabelDemAnejos;
    private javax.swing.JLabel jLabelDemEspumantes;
    private javax.swing.JLabel jLabelDemandaEspumantes;
    private javax.swing.JLabel jLabelDemandaEspumantes1;
    private javax.swing.JLabel jLabelFondo2;
    private javax.swing.JLabel jLabelFondo66;
    private javax.swing.JLabel jLabelMaxBotellasAnejos;
    private javax.swing.JLabel jLabelMaxBotellasJovenes2;
    private javax.swing.JLabel jLabelMinBotellasAnejos;
    private javax.swing.JLabel jLabelMinBotellasJovenes;
    private javax.swing.JLabel jLabelMinBotellasJovenes1;
    private javax.swing.JLabel jLabelOpcionAnejos;
    private javax.swing.JLabel jLabelPUVinoEspumoso;
    private javax.swing.JLabel jLabelPUVinoEspumoso1;
    private javax.swing.JLabel jLabelPUVinoJoven1;
    private javax.swing.JLabel jLabelProdAnejos;
    private javax.swing.JLabel jLabelProdEspumantes;
    private javax.swing.JLabel jLabelPromedioBotellasAnejos;
    private javax.swing.JLabel jLabelPromedioBotellasJovenes;
    private javax.swing.JLabel jLabelPromedioBotellasJovenes1;
    private javax.swing.JLabel jLabelTotalEspumantes;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTableOpcion1;
    private javax.swing.JTable jTableOpcion2;
    private javax.swing.JTable jTableOpcion3;
    private javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextFieldCostoFijo;
    public javax.swing.JTextField jTextFieldCostoVariableVinoAnejo;
    public javax.swing.JTextField jTextFieldCostoVariableVinoEspumoso;
    public javax.swing.JTextField jTextFieldCostoVariableVinoJoven;
    public javax.swing.JTextField jTextFieldDemAnejos;
    public javax.swing.JTextField jTextFieldDemEspumante;
    public javax.swing.JTextField jTextFieldDemJoven;
    private javax.swing.JTextField jTextFieldM3Dos;
    private javax.swing.JTextField jTextFieldM3Tres;
    private javax.swing.JTextField jTextFieldM3Una;
    private javax.swing.JTextField jTextFieldMaxAnejos;
    private javax.swing.JTextField jTextFieldMaxJovenes;
    private javax.swing.JTextField jTextFieldMinAnejos;
    private javax.swing.JTextField jTextFieldMinJovenes;
    public javax.swing.JTextField jTextFieldPUVinoAnejo;
    public javax.swing.JTextField jTextFieldPUVinoEspumoso;
    public javax.swing.JTextField jTextFieldPUVinoJoven;
    private javax.swing.JTextField jTextFieldProbDos;
    private javax.swing.JTextField jTextFieldProbTres;
    private javax.swing.JTextField jTextFieldProbUno;
    public javax.swing.JTextField jTextFieldProdAnejos;
    public javax.swing.JTextField jTextFieldProdEspumante;
    public javax.swing.JTextField jTextFieldProdJoven;
    private javax.swing.JTextField jTextFieldPromedioAnejos;
    private javax.swing.JTextField jTextFieldPromedioJovenes;
    public javax.swing.JTextField jTextFieldTotalOpcionDos;
    public javax.swing.JTextField jTextFieldTotalOpcionTres;
    public javax.swing.JTextField jTextFieldTotalOpcionUno;
    private java.awt.Label labelCVVinoJoven;
    private java.awt.Label labelDemandaAnejos;
    private java.awt.Label labelDemandaEspumante4;
    private java.awt.Label labelDemandaJovenes;
    private java.awt.Label labelFunciones;
    private java.awt.Label labelOpcion1;
    private java.awt.Label labelOpcion2;
    private java.awt.Label labelOpcion3;
    private java.awt.Label lavelProduccion;
    private java.awt.Label lavelResultado;
    private java.awt.Label lavelTotalResultado;
    // End of variables declaration//GEN-END:variables
}
